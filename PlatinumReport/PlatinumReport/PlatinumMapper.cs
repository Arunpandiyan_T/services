﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using System.Reflection;
using PlatinumDataAccessLayer;


namespace PlatinumReport
{
    public class PlatinumMapper<TSource, TDestination>
        where TSource : class
        where TDestination : class
    {
        public PlatinumMapper()
        {
            //From entity to Model
            Mapper.CreateMap<SearchDeviceHistory, PlatinumReports.Models.SearchDeviceHistory>();
            Mapper.CreateMap<SearchDeviceProfile, PlatinumReports.Models.SearchDeviceProfile>();
            Mapper.CreateMap<DeviceEntity, PlatinumReports.Models.DeviceEntity>();
            Mapper.CreateMap<EventEntity, PlatinumReports.Models.EventEntity>();
            Mapper.CreateMap<SearchEventHistory, PlatinumReports.Models.SearchEventHistory>();
           Mapper.CreateMap<DellReport, PlatinumReports.Models.DellReportModel>();

            //From model to entity
            Mapper.CreateMap<PlatinumReports.Models.SearchDeviceHistory, SearchDeviceHistory>();
            Mapper.CreateMap<PlatinumReports.Models.SearchDeviceProfile, SearchDeviceProfile>();
            Mapper.CreateMap<PlatinumReports.Models.DeviceEntity, DeviceEntity>();
            Mapper.CreateMap<PlatinumReports.Models.EventEntity, EventEntity>();
            Mapper.CreateMap<PlatinumReports.Models.SearchEventHistory, SearchEventHistory>();
            Mapper.CreateMap<PlatinumReports.Models.DellReportModel, DellReport>();

        }

        public TDestination Translate(TSource obj)
        {
            return Mapper.Map<TDestination>(obj);
        }
        public IEnumerable<TDestination> TranslateList(IEnumerable<TSource> sObj)
        {
            return Mapper.Map<IEnumerable<TSource>, IEnumerable<TDestination>>(sObj);
        }
        public IEnumerable<TDestination> TranslateI(IList<TSource> sObj)
        {
            return Mapper.Map<IList<TSource>, IList<TDestination>>(sObj);
        }
    }
}