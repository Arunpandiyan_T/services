﻿using PlatinumDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PlatinumReport.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Index Action retrieve details about all the actions  
        /// </summary>
        /// <returns>Returns the list of all actions from database. </returns>
        public ActionResult Index()
        {
            try
            {
                PlatinumRepository repo = new PlatinumRepository();
                var reportDetails = repo.GetReportDetails();
                return View(reportDetails);
            }
            catch
            {
                return View();
            }
        }

    }
}