﻿using Newtonsoft.Json;
using PlatinumDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PlatinumReport.Controllers
{
    public class ReportController : Controller
    {

        /// <summary>
        /// Display the Control to get the device profile based on service tag and date.
        /// </summary>
        /// <param name="reportName">Name of the Report</param>
        /// <returns>returns view on the page.</returns>
        [HttpGet]
        public ActionResult GetDeviceProfile(string reportName)
        {
            ViewBag.ReportName = reportName;
            return View();
        }

        /// <summary>
        /// get the list of all device profile based on service tag and date.
        /// </summary>
        /// <param name="model">Model obj contains service tag and date provided by end-user to get the device profile</param>
        /// <returns>List of Device Profile data.</returns>
        public JsonResult GetDeviceProfileData(string model)
        {
            if (ModelState.IsValid)
            {
                PlatinumReports.Models.SearchDeviceProfile deviceObj = JsonConvert.DeserializeObject<PlatinumReports.Models.SearchDeviceProfile>(model);
                DeviceEntity deviceProfile = null;
                try
                {
                    PlatinumMapper<PlatinumReports.Models.SearchDeviceProfile, SearchDeviceProfile> mapObj = new PlatinumMapper<PlatinumReports.Models.SearchDeviceProfile, SearchDeviceProfile>();
                    PlatinumMapper<DeviceEntity, PlatinumReports.Models.DeviceEntity> mapDeviceObj = new PlatinumMapper<DeviceEntity, PlatinumReports.Models.DeviceEntity>();
                    var dal = new PlatinumRepository();
                    SearchDeviceProfile newSearchDeviceProfile = mapObj.Translate(deviceObj);
                    deviceProfile = dal.GetDeviceProfile(newSearchDeviceProfile);
                    var modelsDeviceProfile = mapDeviceObj.Translate(deviceProfile);
                    if (modelsDeviceProfile != null)
                    {
                        return Json(modelsDeviceProfile, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(null, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Display the Control to get the device history based on service tag, from date and to date.
        /// </summary>
        /// <param name="reportName">Name of the Report</param>
        /// <returns>returns view on the page.</returns>
        public ActionResult GetDeviceHistory(string reportName)
        {
            ViewBag.ReportName = reportName;
            return View();
        }

        /// <summary>
        /// get the list of all device history based on service tag and date.
        /// </summary>
        /// <param name="model">Model obj contains service tag,from date and to date provided by end-user to get the device history</param>
        /// <returns>List of Device history data.</returns>
        public JsonResult GetDeviceHistoryData(string model)
        {
            if (ModelState.IsValid)
            {
                PlatinumReports.Models.SearchDeviceHistory deviceObj = JsonConvert.DeserializeObject<PlatinumReports.Models.SearchDeviceHistory>(model);
                IEnumerable<DeviceEntity> deviceList = null;
                try
                {
                    PlatinumMapper<PlatinumReports.Models.SearchDeviceHistory, SearchDeviceHistory> mapObj = new PlatinumMapper<PlatinumReports.Models.SearchDeviceHistory, SearchDeviceHistory>();
                    PlatinumMapper<DeviceEntity, PlatinumReports.Models.DeviceEntity> mapList = new PlatinumMapper<DeviceEntity, PlatinumReports.Models.DeviceEntity>();
                    var dal = new PlatinumRepository();
                    var newSearchDeviceHistory = mapObj.Translate(deviceObj);
                    deviceList = dal.GetDeviceHistory(newSearchDeviceHistory);
                    var modelsDeviceList = mapList.TranslateList(deviceList);
                    if (modelsDeviceList != null)
                    {
                         return Json(modelsDeviceList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(modelsDeviceList, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(ex.InnerException);
                }
            }
            else
            {
                return Json(null);
            }
        }
        /// <summary>
        /// Display the Control to get the Event history based on service tag, from date and to date.
        /// </summary>
        /// <param name="reportName">Name of the Report</param>
        /// <returns>returns view on the page.</returns>
        [HttpGet]
        public ActionResult GetEventHistory(string reportName)
        {
            ViewBag.ReportName = reportName;
            return View();
        }

        /// <summary>
        /// get the list of all Event history based on service tag and date.
        /// </summary>
        /// <param name="model">Model obj contains service tag,from date and to date provided by end-user to get the Event history</param>
        /// <returns>List of Device history data.</returns>
        public ActionResult GetEventHistoryData(string model)
        {
            if (ModelState.IsValid)
            {
                PlatinumReports.Models.SearchEventHistory eventObj = JsonConvert.DeserializeObject<PlatinumReports.Models.SearchEventHistory>(model);
                IEnumerable<EventEntity> eventList = null;
                try
                {
                    PlatinumMapper<PlatinumReports.Models.SearchEventHistory, SearchEventHistory> mapObj = new PlatinumMapper<PlatinumReports.Models.SearchEventHistory, SearchEventHistory>();
                    PlatinumMapper<EventEntity, PlatinumReports.Models.EventEntity> mapList = new PlatinumMapper<EventEntity, PlatinumReports.Models.EventEntity>();
                    var dal = new PlatinumRepository();
                    var newSearchEventHistory = mapObj.Translate(eventObj);
                    eventList = dal.GetEventHistory(newSearchEventHistory);

                    var modelsEventList = mapList.TranslateList(eventList);
                    if (modelsEventList != null)
                    {
                        return View(modelsEventList);
                    }
                    else
                    {
                        return View();
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.InnerException);
                }
            }
            else
            {
                return View();
            }
        }

        /// <summary>
        /// Retrieve the details about the Reports
        /// </summary>
        /// <returns>List of Reports and its details</returns>
        public ActionResult GetReports()
        {
            PlatinumRepository dal = new PlatinumRepository();
            var data = dal.GetReportDetails();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}
