﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlatinumReports.Models
{
    /// <summary>
    /// Device Entity class contains all the properties for Device related data
    /// </summary>
    public class DeviceEntity
    {
        public string ApiKey { get; set; }
        public string BN { get; set; }
        public string BR { get; set; }
        public string BV { get; set; }
        public string CPU { get; set; }
        public string DNV { get; set; }
        public string EULA { get; set; }
        public string GeoId { get; set; }
        public string IpAddress { get; set; }
        public string IsTestDevice { get; set; }
        public string LC { get; set; }
        public string OSID { get; set; }
        public string osother { get; set; }
        public string OsType { get; set; }
        public string OSVER { get; set; }
        public string PeaVal { get; set; }
        public string PID { get; set; }
        public string PN { get; set; }
        public string ST { get; set; }
        public DateTime TS { get; set; }
    }
}

