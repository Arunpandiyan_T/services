﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PlatinumReports.Models
{
    /// <summary>
    /// Contains the property for searching device profile
    /// </summary>
    public class SearchDeviceProfile
    {
        [Required]
        public string ServiceTag { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime date { get; set; }

    }
}