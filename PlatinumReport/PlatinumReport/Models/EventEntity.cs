﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlatinumReports.Models
{
    /// <summary>
    /// Event Entity class contains all the properties for Event related data
    /// </summary>
    public class EventEntity
    {
        public string ApiKey { get; set; }
        public string ApplicationId { get; set; }
        public string AppVersion { get; set; }
        public string ApplicationName { get; set; }
        public string EventReceivedUtc { get; set; }
        public string EventDateTime { get; set; }
        public string EventTrackingCode { get; set; }
        public string EventTypeId { get; set; }
        public string ST { get; set; }
        public DateTime TS { get; set; }
        public string EventTypeName { get; set; }

    }
}