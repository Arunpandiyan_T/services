﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace PlatinumReports.Models
{
    /// <summary>
    /// Contains the property for searching device history
    /// </summary>
    public class SearchDeviceHistory
    {

        public string ServiceTag { get; set; }
        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }
    }
}