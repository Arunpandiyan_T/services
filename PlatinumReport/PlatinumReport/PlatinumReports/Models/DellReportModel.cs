﻿namespace PlatinumReports.Models
{
    /// <summary>
    /// contains the properties of a report
    /// </summary>
    public class DellReportModel
    {
        public int Report_Id { get; set; }
        public string ReportHeader { get; set; }
        public string ReportDescription { get; set; }
        public string ViewName { get; set; }
        public string ControllerName { get; set; }
        public string OtherInfo { get; set; }
    }
}