﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PlatinumDataAccessLayer
{
    /// <summary>
    /// Contains the property for searching device profile
    /// </summary>
    public class SearchDeviceProfile
    {
        public string ServiceTag { get; set; }
        [DataType(DataType.Date)]
        public DateTime date { get; set; }

    }
}