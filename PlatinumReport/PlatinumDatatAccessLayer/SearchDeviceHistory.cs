﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatinumDataAccessLayer
{
    /// <summary>
    /// Contains the property for searching device history
    /// </summary>
    public class SearchDeviceHistory
    {
        public string ServiceTag { get; set; }
        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }
    }
}
