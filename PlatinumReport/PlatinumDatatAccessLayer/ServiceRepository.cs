﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;


namespace PlatinumDataAccessLayer
{
    public class ServiceRepository
    {
        /// <summary>
        /// Property for sending HTTP request for web api.
        /// </summary>
        public HttpClient Client { get; set; }
        
        /// <summary>
        /// returns the url of web api
        /// </summary>
        public ServiceRepository()
        {
            Client = new HttpClient();
            string webUri = ConfigurationManager.ConnectionStrings["WebUrl"].ConnectionString;
            Client.BaseAddress = new Uri(webUri);
        }
        /// <summary>
        /// Provides the response for particular url.
        /// </summary>
        /// <param name="url">Url to fetch the data</param>
        /// <returns>The response of the provided url.</returns>
        public  HttpResponseMessage GetResponse(string url)
        {
            return  Client.GetAsync(url).Result;
        }
        
    }
}