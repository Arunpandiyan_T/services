﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;


namespace PlatinumDataAccessLayer
{
    public class PlatinumRepository
    {
        /// <summary>
        /// object for retrieving data from web-api
        /// </summary>
        ServiceRepository serviceRepository = new ServiceRepository();

        /// <summary>
        /// Fetches data from Web-Api regarding events for a specific period of time.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>List of events based on service tag, to date and from date</returns>
        public IEnumerable<EventEntity> GetEventHistory(SearchEventHistory obj)
        {
            IEnumerable<EventEntity> eventList = null;
            HttpResponseMessage response = serviceRepository.GetResponse("api/Service/GetEventHistory?serviceTag=" + obj.ServiceTag.ToString() + "&fromDate=" + obj.FromDate.ToString() + "&toDate=" + obj.ToDate.ToString());
            try
            {
                response.EnsureSuccessStatusCode();
                eventList = response.Content.ReadAsAsync<IEnumerable<EventEntity>>().Result;
            }
            catch (Exception)
            {
                throw;
            }
            return eventList;
        }

        /// <summary>
        /// Fetches data from Web-Api regarding Device for a specific period of time.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>List of device events based on service tag, to date and from date</returns>
        public IEnumerable<DeviceEntity> GetDeviceHistory(SearchDeviceHistory obj)
        {
            IEnumerable<DeviceEntity> deviceList = null;

            HttpResponseMessage response = serviceRepository.GetResponse("api/Service/GetDeviceHistory?serviceTag=" + obj.ServiceTag.ToString() + "&fromDate=" + obj.FromDate.ToString() + "&toDate=" + obj.ToDate.ToString());
            try
            {
                response.EnsureSuccessStatusCode();
                deviceList = response.Content.ReadAsAsync<IEnumerable<DeviceEntity>>().Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return deviceList;
        }
        /// <summary>
        /// Fetches data from Web-Api reagrding Device profile for a specific date.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>List of events based on service tag  and date</returns>
        public DeviceEntity GetDeviceProfile(SearchDeviceProfile obj)
        {
            DeviceEntity deviceEntity = null;
            try
            {
                HttpResponseMessage response = serviceRepository.GetResponse("api/Service/GetDeviceProfile?serviceTag=" + obj.ServiceTag.ToString() + "&date=" + obj.date.ToString());
                response.EnsureSuccessStatusCode();
                deviceEntity = response.Content.ReadAsAsync<DeviceEntity>().Result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return deviceEntity;
        }
        /// <summary>
        /// Get details about the reports from database.
        /// </summary>
        /// <returns>List of reports</returns>
        public IEnumerable<DellReport> GetReportDetails()
        {
            IEnumerable<DellReport> reportList = null;
            DellServicesEntities db = new DellServicesEntities();
            try
            {
                reportList = (from data in db.DellReports select data).AsEnumerable<DellReport>();

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return reportList;
        }

       
    }
}
