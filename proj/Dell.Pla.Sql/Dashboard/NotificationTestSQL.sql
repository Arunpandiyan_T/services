﻿CREATE TABLE [Dashboard].[NotificationTestSQL](
	NotificationTestSQLId [int] IDENTITY(1,1) NOT NULL primary key,
	[WinEndTime] [datetime] NOT NULL,
	[SWB] [varchar](max) NOT NULL,
	cntDownloadSuccess [float] NULL,
	cntDownload [float] NULL,
	cntInstallationSuccess [float] NULL,
	cntInstallation [float] NULL
)