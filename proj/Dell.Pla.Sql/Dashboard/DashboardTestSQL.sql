﻿CREATE TABLE [Dashboard].[DashboardTestSQL](
	[WinEndTime] [datetime] NOT NULL,
	[ApplicationId] [bigint] NOT NULL,
	[AppMessages] [float] NULL,
	[CntAppCrashes] [float] NULL,
	cntDownloadSuccess [float] NULL,
	cntDownload [float] NULL,
	cntInstallationSuccess [float] NULL,
	cntInstallation [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC,
	[WinEndTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)