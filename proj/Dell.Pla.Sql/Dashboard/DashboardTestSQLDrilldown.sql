﻿CREATE TABLE [Dashboard].[DashboardTestSQLDrilldown](
	[DashboardTestSQLId] [int] IDENTITY(1,1) NOT NULL,
	[WinEndTime] [datetime] NOT NULL,
	[ApplicationId] [bigint] NOT NULL,
	[SWB] [nvarchar](max) NULL,
	[CPU] [nvarchar](max) NULL,
	[BN] [nvarchar](max) NULL,
	[BV] [nvarchar](max) NULL,
	ItemName [nvarchar](max) NULL,
	[AppMessages] [float] NULL,
	[CntAppCrashes] [float] NULL,
	cntDownloadSuccess [float] NULL,
	cntDownload [float] NULL,
	cntInstallationSuccess [float] NULL,
	cntInstallation [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[DashboardTestSQLId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)