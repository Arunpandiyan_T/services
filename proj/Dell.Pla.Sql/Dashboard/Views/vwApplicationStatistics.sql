﻿create view Dashboard.vwApplicationStatistics
as
select ApplicationId
		,totMessages = sum(AppMessages)
		,cntAppCrashes = sum(CntAppCrashes)
		,cntDownloadSuccess = sum(cntDownloadSuccess), cntDownload = sum(cntDownload)
		,DownloadRate = case when sum(cntDownload) <> 0 then 1.0 * sum(cntDownloadSuccess) / sum(cntDownload) end
		, cntInstallationSuccess = sum(cntInstallationSuccess), cntInstallation = sum(cntInstallation)
		,InstallationRate = case when sum(cntInstallation) <> 0 then 1.0 * sum(cntInstallationSuccess) / sum(cntInstallation) end
 from [Dashboard].DashboardTestSQLDrilldown
 where [WinEndTime] > dateadd( day, -1, getutcdate() )
 group by ApplicationId