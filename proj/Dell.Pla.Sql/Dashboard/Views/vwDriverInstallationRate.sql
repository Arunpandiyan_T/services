﻿create view Dashboard.vwDriverInstallationRate
as
select SWB
		,ApplicationId = case when min(ApplicationId) = max(ApplicationId) then max(ApplicationId) end
		,ItemName = max( ItemName )
		,cntDownloadSuccess = sum(cntDownloadSuccess), cntDownload = sum(cntDownload)
		,DownloadRate = case when sum(cntDownload) <> 0 then 1.0 * sum(cntDownloadSuccess) / sum(cntDownload) end
		, cntInstallationSuccess = sum(cntInstallationSuccess), cntInstallation = sum(cntInstallation)
		,InstallationRate = case when sum(cntInstallation) <> 0 then 1.0 * sum(cntInstallationSuccess) / sum(cntInstallation) end
 from [Dashboard].[DashboardTestSQLDrilldown]
 where [WinEndTime] > dateadd( day, -1, getutcdate() )
	and len(SWB) > 1 -- for now
 group by SWB
 having sum(cntDownload) + sum(cntInstallation) > 0 