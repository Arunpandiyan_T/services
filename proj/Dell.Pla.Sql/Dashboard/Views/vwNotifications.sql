﻿create view Dashboard.vwNotifications
as
SELECT [WinEndTime]
      ,[SWB]
      ,[cntDownloadSuccess]
      ,[cntDownload]
      ,[cntInstallationSuccess]
      ,[cntInstallation]
      ,DownloadRate = case when cntDownload <> 0 then 1.0 * cntDownloadSuccess / cntDownload end
	  ,InstallationRate = case when cntInstallation <> 0 then 1.0 * cntInstallationSuccess / cntInstallation end
  FROM [Dashboard].[NotificationTestSQL]
  WHERE [WinEndTime] > dateadd( day, -1, getutcdate() )