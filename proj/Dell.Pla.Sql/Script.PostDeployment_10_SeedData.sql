﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/




/*
	The order here is important
*/
:r .\SeedData\MetaData.FieldType.sql
:r .\SeedData\MetaData.Field.sql
:r .\SeedData\API.EventHubInfo.sql
:r .\SeedData\Daws.Application.sql
:r .\SeedData\Daws.EventType.sql
