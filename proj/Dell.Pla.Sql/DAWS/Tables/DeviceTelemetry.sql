﻿CREATE TABLE [DAWS].[DeviceTelemetry] (
    [TelemetryId]   INT           IDENTITY (1, 1) NOT NULL,
    [ServiceTag]    CHAR (7)      NOT NULL,
    [ApplicationId] INT           NOT NULL,
    [TelemetryData] VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_DeviceTelemetryId] PRIMARY KEY CLUSTERED ([TelemetryId] ASC)
);