﻿CREATE TABLE [DAWS].[EventTabId] (
    [EventCreatedDate]           DATE           NOT NULL,
    [ServiceTag]                 CHAR (7)       NOT NULL,
    [DeviceVersion]              INT            NOT NULL,
    [EventTypeId]                INT            NOT NULL,
    [ApplicationId]              INT            NOT NULL,
    [EventDateTime]              DATETIME2 (4)  NOT NULL,
    [EventCreatedTime]           TIME (4)       NOT NULL,
    [AppVersion]                 VARCHAR (255)  NULL,
    [Url]                        VARCHAR (512)  NULL,
    [Filename]                   VARCHAR (255)  NULL,
    [PeaVal]                     BIT            NULL,
    [LanguageCultureName]        CHAR (5)       NULL,
    [BiosRev]                    VARCHAR (20)   NULL,
    [GeoId]                      INT            NULL,
    [PlatformId]                 INT            NULL,
    [DefaultBrowserId]           INT            NULL,
    [CpuId]                      INT            NULL,
    [OsTypeId]                   INT            NULL,
    [WindowsName]                VARCHAR (255)  NOT NULL,
    [IpAddress]                  VARCHAR (15)   NULL,
    [DotNetVersionList]          VARCHAR (512)  NULL,
    [DeviceInfoJSON]             VARCHAR (8000) NULL,
    [DeviceRegistrationInfoJSON] VARCHAR (8000) NULL,
    [ApplicationFieldsListJSON]  VARCHAR (8000) NULL,
    [DynamicFieldsListJSON]      VARCHAR (8000) NULL
);


GO
CREATE CLUSTERED COLUMNSTORE INDEX [cci_EventId]
    ON [DAWS].[EventTabId]
    ON [eventDateRangePS] ([EventCreatedDate]);

