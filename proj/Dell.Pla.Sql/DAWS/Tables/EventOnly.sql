﻿CREATE TABLE [DAWS].[EventOnly] (
    [MessageId]                 UNIQUEIDENTIFIER NOT NULL,
    [ServiceTag]                CHAR (7)         NOT NULL,
    [DeviceVersion]             INT              NOT NULL,
    [EventTypeId]               INT              NOT NULL,
    [ApplicationId]             INT              NOT NULL,
    [EventDateTime]             DATETIME2 (4)    NOT NULL,
    [EventCreatedDate]          DATE             NOT NULL,
    [EventCreatedTime]          TIME (4)         NOT NULL,
    [AppVersion]                VARCHAR (255)    NULL,
    [Url]                       VARCHAR (512)    NULL,
    [Filename]                  VARCHAR (255)    NULL,
    [ApplicationFieldsListJSON] VARCHAR (MAX)    NULL,
    [DynamicFieldsListJSON]     VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_EventOnlyServiceTab] PRIMARY KEY CLUSTERED ([EventCreatedDate] ASC, [ServiceTag] ASC, [MessageId] ASC) ON [eventDateRangePS] ([EventCreatedDate]),
    CONSTRAINT [EventOnlyApplication] FOREIGN KEY ([ApplicationId]) REFERENCES [DAWS].[Application] ([ApplicationId]),
    CONSTRAINT [EventOnlyEventType] FOREIGN KEY ([EventTypeId]) REFERENCES [DAWS].[EventType] ([EventTypeId])
);


GO
CREATE NONCLUSTERED INDEX [idx_FilterByApplication]
    ON [DAWS].[EventOnly]([ApplicationId] ASC)
    INCLUDE([ServiceTag], [DeviceVersion])
    ON [eventDateRangePS] ([EventCreatedDate]);


GO
CREATE NONCLUSTERED INDEX [idx_ServiceTagX]
    ON [DAWS].[EventOnly]([ServiceTag] ASC, [DeviceVersion] ASC)
    ON [eventDateRangePS] ([EventCreatedDate]);

