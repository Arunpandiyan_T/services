﻿CREATE TABLE [DAWS].[EventType] (
    [EventTypeId]             INT           IDENTITY (1, 1) NOT NULL,
    [EventTypeName]           VARCHAR (80)  NOT NULL,
    [Description]             VARCHAR (255) NULL,
   -- [StoreAlongDeviceInfo]    BIT           DEFAULT ((0)) NOT NULL,
   -- [StoreAlongDotNetInfo]    BIT           DEFAULT ((0)) NOT NULL,
   -- [StoreAlongDeviceRegInfo] BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_EventTypeId] PRIMARY KEY CLUSTERED ([EventTypeId] ASC),
    CONSTRAINT [EventTypeName] UNIQUE NONCLUSTERED ([EventTypeName] ASC)
);

