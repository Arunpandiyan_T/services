﻿CREATE TABLE [DAWS].[EventIngestionTestData] (
    [MessageId]                    UNIQUEIDENTIFIER NOT NULL,
    [DeviceVersion]                INT              NOT NULL,
    [ST]                           CHAR (7)         NOT NULL,
    [EventTypeId]                  INT              NOT NULL,
    [ApplicationId]                INT              NOT NULL,
    [EventDateTime]                DATETIME2 (4)    DEFAULT (getdate()) NOT NULL,
    [TS]                           DATETIME2 (4)    DEFAULT (getdate()) NOT NULL,
    [AppVersion]                   VARCHAR (255)    NULL,
    [Url]                          NVARCHAR (512)   NULL,
    [Filename]                     NVARCHAR (255)   NULL,
    [FileVersion]                  NVARCHAR (255)   NULL,
    [FileSize]                     BIGINT           NULL,
    [ExitCode]                     INT              NULL,
    [ItemName]                     NVARCHAR (255)   NULL,
    [UploadedFilename]             NVARCHAR (255)   NULL,
    [AdditionalData1]              NVARCHAR (4000)  NULL,
    [AdditionalData2]              NVARCHAR (4000)  NULL,
    [EventTrackingCode]            NVARCHAR (40)    NULL,
    [IpAddress]                    VARCHAR (15)     NULL,
    [DynamicApplicationFieldsJSON] NVARCHAR (MAX)   NULL,
    [IsTestDevice]                 BIT              DEFAULT ((0)) NOT NULL,
    [PN]                           NVARCHAR (512)   NULL,
    [PID]                          VARCHAR (4)      NULL,
    [EULA]                         BIT              DEFAULT ((0)) NOT NULL,
    [LC]                           NCHAR (5)        NULL,
    [BN]                           NVARCHAR (80)    NULL,
    [BV]                           NVARCHAR (80)    NULL,
    [OsType]                       INT              NULL,
    [OSVER]                        NVARCHAR (512)   NULL,
    [osother]                      NVARCHAR (255)   NULL,
    [GbF]                          INT              NULL,
    [M]                            INT              NULL,
    [OCD]                          DATETIME2 (7)    NULL,
    [BR]                           NVARCHAR (20)    NULL,
    [CPU]                          NVARCHAR (255)   NULL,
    [GeoId]                        INT              NULL,
    [DNV]                          NVARCHAR (512)   NULL,
    [DynamicDeviceFieldsJSON]      NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_IngestionTestData] PRIMARY KEY CLUSTERED ([TS] ASC, [ST] ASC, [MessageId] ASC) ON [eventDatetimeRangePS] ([TS])
);


GO
CREATE NONCLUSTERED INDEX [idx_MessageId_Ingestion]
    ON [DAWS].[EventIngestionTestData]([MessageId] ASC)
    ON [eventDatetimeRangePS] ([TS]);

