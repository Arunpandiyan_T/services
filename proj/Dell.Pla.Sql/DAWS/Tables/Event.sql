CREATE TABLE [DAWS].[Event] (
    [MessageId]                  UNIQUEIDENTIFIER NOT NULL,
	DeviceVersion				 INT              NOT NULL,
    [ServiceTag]                 CHAR (7)         NOT NULL,
    [EventTypeId]                INT              NOT NULL,
    [ApplicationId]              INT              NOT NULL,
    [EventDateTime]              DATETIME2 (4)    DEFAULT (getdate()) NOT NULL,
    [EventCreatedDate]           DATE             DEFAULT (getdate()) NOT NULL,
    [EventCreatedTime]           TIME (4)         DEFAULT (getdate()) NOT NULL,
    [AppVersion]                 VARCHAR (255)    NULL,
    [Url]                        VARCHAR (512)    NULL,
    [Filename]                   VARCHAR (255)    NULL,
    [DotNetVersionList]          VARCHAR (512)    NULL,
    [DeviceInfoJSON]             VARCHAR (MAX)    NULL,
    [DeviceRegistrationInfoJSON] VARCHAR (MAX)    NULL,
    [ApplicationFieldsListJSON]  VARCHAR (MAX)    NULL,
    [DynamicFieldsListJSON]      VARCHAR (MAX)    NULL,
	IncludeDeviceInfo as (case when DeviceRegistrationInfoJSON is not null then 1 else 0 end),
    CONSTRAINT [PK_EventServiceTab] PRIMARY KEY CLUSTERED ([EventCreatedDate] ASC, [ServiceTag] ASC, [MessageId] ASC) ON [eventDateRangePS] ([EventCreatedDate]),
    CONSTRAINT [EventApplication] FOREIGN KEY ([ApplicationId]) REFERENCES [DAWS].[Application] ([ApplicationId]),
    CONSTRAINT [EventEventType] FOREIGN KEY ([EventTypeId]) REFERENCES [DAWS].[EventType] ([EventTypeId])
);


GO

CREATE NONCLUSTERED INDEX [idx_ServiceTag]
    ON [DAWS].[Event]([ServiceTag] ASC, DeviceVersion) INCLUDE ( IncludeDeviceInfo )
    ON [eventDateRangePS] ([EventCreatedDate]);
GO
CREATE NONCLUSTERED INDEX [idx_MessageId]
    ON [DAWS].[Event]([MessageId] ASC)
    ON [eventDateRangePS] ([EventCreatedDate]);

