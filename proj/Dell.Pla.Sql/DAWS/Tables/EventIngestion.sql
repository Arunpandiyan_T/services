﻿CREATE TABLE [DAWS].[EventIngestion] (
    [MessageId]             UNIQUEIDENTIFIER NOT NULL,
    [DeviceVersion]         INT              NOT NULL,
    ST				        CHAR (7)         NOT NULL,  -- ServiceTag
    [EventTypeId]           INT              NOT NULL,
    [ApplicationId]         INT              NOT NULL,
    [EventDateTime]         DATETIME2 (4)    DEFAULT (getdate()) NOT NULL,
    TS					    DATETIME2 (4)    DEFAULT (getdate()) NOT NULL, -- EventCreatedDate
    [AppVersion]            VARCHAR (255)    NULL,
    [Url]                   NVARCHAR (512)    NULL,
    [Filename]              NVARCHAR (255)    NULL,
    [FileVersion]           NVARCHAR (255)    NULL,
    [FileSize]              BIGINT           NULL,
    [ExitCode]              INT              NULL,
    [ItemName]              NVARCHAR (255)    NULL,
    [UploadedFilename]      NVARCHAR (255)    NULL,
    [AdditionalData1]       NVARCHAR (4000)   NULL,
    [AdditionalData2]       NVARCHAR (4000)   NULL,
    [EventTrackingCode]     NVARCHAR (40)     NULL,
    [IpAddress]             NVARCHAR (15)     NULL,
    [DynamicApplicationFieldsJSON] NVARCHAR (MAX)    NULL,
  -- device section
	[IsTestDevice] [bit] NOT NULL DEFAULT(0),
	[PN] nvarchar(512) NULL,
	[PID] [varchar](4) NULL,
	[EULA] [bit] NOT NULL DEFAULT(0),
	[LC] nchar(5) NULL,
	[BN] nvarchar(80) NULL,
	[BV] nvarchar(80) NULL,
	[OsType] Int NULL,
	[OSVER] nvarchar(512) NULL,
	[osother] nvarchar(255) NULL,
	[GbF] [int] NULL,
	[M] [int] NULL,
	[OCD] [datetime2](7) NULL,
	[BR] nvarchar(20) NULL,
	[CPU] nvarchar(255) NULL,
	[GeoId] [int] NULL,
	[DNV] nvarchar(512) NULL,

	[DynamicDeviceFieldsJSON] nvarchar(max) NULL,

    CONSTRAINT [PK_Ingestion] PRIMARY KEY CLUSTERED (TS ASC, ST ASC, [MessageId] ASC) ON [eventDatetimeRangePS] (TS)
);
go
CREATE NONCLUSTERED INDEX idx_MessageId_Ingestion
    ON DAWS.EventIngestion([MessageId] ASC)
    ON [eventDatetimeRangePS] (TS);
