﻿CREATE TABLE [DAWS].[Device] (
    [ServiceTag]                 CHAR (7)      NOT NULL,
    [LastUpdateApplicationId]    INT           NOT NULL,
    [LastUpdateDateTime]         DATETIME2 (4) DEFAULT (getutcdate()) NOT NULL,
    [DeviceCreatedDateTime]      DATETIME2 (4) DEFAULT (getutcdate()) NOT NULL,
    [IsTestDevice]               BIT           NOT NULL,
    [DotNetVersionList]          VARCHAR (512) NULL,
    [DeviceInfoJSON]             VARCHAR (MAX) NULL,
    [DeviceRegistrationInfoJSON] VARCHAR (MAX) NULL,
    [DeviceVersion]              INT           NOT NULL,
	[DynamicDeviceFieldsJSON]    VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_deviceServiceTag] PRIMARY KEY CLUSTERED ([ServiceTag] ASC),
    CONSTRAINT [DeviceLastUpdateApplication] FOREIGN KEY ([LastUpdateApplicationId]) REFERENCES [DAWS].[Application] ([ApplicationId])
);
GO
CREATE NONCLUSTERED INDEX [idx_VersionServiceTag]
    ON [DAWS].[Device]([DeviceVersion] ASC, [ServiceTag] ASC);

