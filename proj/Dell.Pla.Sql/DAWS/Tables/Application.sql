﻿CREATE TABLE [DAWS].[Application] (
    [ApplicationId]          INT           IDENTITY (1, 1) NOT NULL,
    [ApplicationName]        VARCHAR (80)  NOT NULL,
    [Description]            VARCHAR (255) NULL,
    [ApiKey]                 CHAR (36)     DEFAULT (newid()) NOT NULL,
    [SecretKey]              CHAR (36)     DEFAULT (newid()) NOT NULL,
    [MayUpdateDeviceDetails] BIT           DEFAULT ((0)) NOT NULL,
    [isEnabled]              BIT           DEFAULT ((0)) NULL,
	onboardingTime			 datetime NULL,
    CONSTRAINT [PK_ApplicationId] PRIMARY KEY CLUSTERED ([ApplicationId] ASC),
    CONSTRAINT [ApplicationName] UNIQUE NONCLUSTERED ([ApplicationName] ASC)
);



