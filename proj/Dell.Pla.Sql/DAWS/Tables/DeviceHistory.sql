﻿CREATE TABLE [DAWS].[DeviceHistory] (
    [ServiceTag]                 CHAR (7)      NOT NULL,
    [DeviceVersion]              INT           NOT NULL,
    [DeviceRegistrationInfoJSON] VARCHAR (MAX) NULL,
    [DotNetVersionList]          VARCHAR (512) NULL,
    [DeviceInfoJSON]             VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([ServiceTag] ASC, [DeviceVersion] ASC)
);

