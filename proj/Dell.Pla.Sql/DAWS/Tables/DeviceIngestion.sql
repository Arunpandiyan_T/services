﻿CREATE TABLE [DAWS].DeviceIngestion (
	[ServiceTag] [char](7) NOT NULL primary key,
	[DeviceVersion] [int] NOT NULL DEFAULT(1),
	[LastUpdateApplicationId] [int] NOT NULL,
	[LastUpdateDateTime] [datetime2](7) NOT NULL DEFAULT (getUtcDate()),
	[DeviceCreatedDateTime] [datetime2](7) NOT NULL DEFAULT (getUtcDate()),

	[IsTestDevice] [bit] NOT NULL DEFAULT(0),
	[PN] nvarchar(512) NULL,
	[PID] varchar(4) NULL,
	[EULA] [bit] NOT NULL DEFAULT(0),
	[LC] nchar(5) NULL,
	[BN] nvarchar(80) NULL,
	[BV] nvarchar(80) NULL,
	[OsType] Int NULL,
	[OSVER] nvarchar(512) NULL,
	[osother] nvarchar(255) NULL,
	[GbF] [int] NULL,
	[M] [int] NULL,
	[OCD] [datetime2](7) NULL,
	[BR] nvarchar(20) NULL,
	[CPU] nvarchar(255) NULL,
	[GeoId] [int] NULL,
	[DNV] nvarchar(512) NULL,

	[DynamicDeviceFieldsJSON] nvarchar(max) NULL,

	-- we can keep in line in DAWS.DeviceIngestion the devices that sent some messages in the last 4/6 months
	-- and save everything else in a columnstore
	-- if the device comes back, the first message (treated as new device) may have less device details, then a procedure can be called to retrieve and merge
	-- the device record
)
