﻿CREATE view [BI].[vwEventsExtended]
as
SELECT  l.*
	  ,dynamicAutoCheckJSON -- = convert(varchar(max), dynamicAutoCheckJSON  case when dynamicAutoCheckJSON = '[{}]' then NULL else SUBSTRING( dynamicAutoCheckJSON, 2, LEN(dynamicAutoCheckJSON)-2 ) end)
	  ,dynamicIsCommericalJSON -- = convert(varchar(max), case when dynamicIsCommericalJSON = '[{}]' then NULL else SUBSTRING( dynamicIsCommericalJSON, 2, LEN(dynamicIsCommericalJSON)-2 ) end)
 FROM (
SELECT
       [ServiceTag]
      ,[EventTypeId]
      ,[ApplicationId]
      ,[EventDateTime]
      ,[EventCreatedDate]
      ,[EventCreatedTime]
	  ,[EventCreatedDateTime] = convert(datetime2, convert(varchar(20),[EventCreatedDate]) +' '+ convert(varchar(20),[EventCreatedTime]) )
      ,[AppVersion]
      ,[Url]
      ,[Filename]
      --,[DeviceInfoJSON]
      ,PlatformName = JSON_VALUE( [DeviceInfoJSON], '$.PN' )
      ,IPAddress = JSON_VALUE( [DeviceInfoJSON], '$.IpAddress' )
      ,PeaVal = JSON_VALUE( [DeviceInfoJSON], '$.PeaVal' )
      ,LanguageCultureName = JSON_VALUE( [DeviceInfoJSON], '$.LC' )
      ,BiosRev = JSON_VALUE( [DeviceInfoJSON], '$.BR' )
      ,OsVersion = JSON_VALUE( [DeviceInfoJSON], '$.OSVER' )
      ,OsType = JSON_VALUE( [DeviceInfoJSON], '$.OsType' )
      ,BrowserName = JSON_VALUE( [DeviceInfoJSON], '$.BN' )
      ,BrowserVersion = JSON_VALUE( [DeviceInfoJSON], '$.BV' )
      ,GeoId = JSON_VALUE( [DeviceInfoJSON], '$.GeoId' )
      ,SystemRamMb = JSON_VALUE( [DeviceInfoJSON], '$.M' )
      --,[DeviceRegistrationInfoJSON]
      ,OobeCompleteDate = JSON_VALUE( [DeviceRegistrationInfoJSON], '$.OCD' )
      ,HardDriveGbFree  = JSON_VALUE( [DeviceRegistrationInfoJSON], '$.GbF' )
      ,Cpu = JSON_VALUE( [DeviceRegistrationInfoJSON], '$.CPU' )
      ,OsOtherId = JSON_VALUE( [DeviceRegistrationInfoJSON], '$.osother' )
      ,[DotNetVersionList]
      --,[ApplicationFieldsListJSON]
      ,FileVersion  = JSON_VALUE( [ApplicationFieldsListJSON], '$.FileVersion' )
      ,FileSize = JSON_VALUE( [ApplicationFieldsListJSON], '$.FileSize' )
      ,ExitCode = JSON_VALUE( [ApplicationFieldsListJSON], '$.ExitCode' )
      ,ItemName = JSON_VALUE( [ApplicationFieldsListJSON], '$.ItemName' )
      ,UploadedFileName = JSON_VALUE( [ApplicationFieldsListJSON], '$.UploadedFileName' )
      ,EventTrackingCode = JSON_VALUE( [ApplicationFieldsListJSON], '$.EventTrackingCode' )
      --,[DynamicFieldsListJSON]
      ,AdditionalData1 = JSON_VALUE( [DynamicFieldsListJSON], '$.Add1' )
      ,AdditionalData2 = JSON_VALUE( [DynamicFieldsListJSON], '$.Add2' )
  FROM (
		SELECT e.[ServiceTag]
			  ,e.[EventTypeId]
			  ,e.[ApplicationId]
			  ,e.[EventDateTime]
			  ,e.[EventCreatedDate]
			  ,e.[EventCreatedTime]
			  ,e.[AppVersion]
			  ,e.[Url]
			  ,e.[Filename]
			  ,[DotNetVersionList] = isnull( e.[DotNetVersionList]  , dev.[DotNetVersionList] )
			  ,[DeviceInfoJSON] = isnull( e.[DeviceInfoJSON]  , dev.[DeviceInfoJSON] )
			  ,[DeviceRegistrationInfoJSON] = isnull( e.[DeviceRegistrationInfoJSON]  , dev.[DeviceRegistrationInfoJSON] )
			  ,[ApplicationFieldsListJSON]
			  ,[DynamicFieldsListJSON]
		 from [DAWS].[Event] e with (nolock)
	     JOIN DAWS.Device dev on dev.ServiceTag = e.ServiceTag
	) x
 ) l
	  cross apply (
			-- NOTE: not all fields are mandatory, if AutoCheckEnabled is not specified we loose the record, we can improve the test...
		  select *
		    from (  SELECT AutoCheckEnabled = min([1]), InstallType = min([2]), LastPingDate = min([3]), OobeDate = min([4]), BIOSPasswordSet = min([5]), InstalledByMyDell= min([6]), ApparentOrphanCase= min([7])
			  FROM  (
				 select ID, Val = substring( Val, patindex( '%:%', Val )+1, Len(val) )
					 from Util.udf_Split( case when l.AdditionalData1 like 'AutoCheckEnabled%' then l.AdditionalData1 end , ';')
					) l
			  PIVOT (min(Val)
				FOR Id in ([1], [2], [3], [4], [5], [6], [7])
				) as p
			 ) s
		    for JSON AUTO
			) jchk ( dynamicAutoCheckJSON )
	  cross apply (
		  select *
		    from (  SELECT InstallType = min([1]), IsCommercial = min([2])
			  FROM  (
				 select ID, Val = substring( Val, patindex( '%:%', Val )+1, Len(val) )
					 from Util.udf_Split( case when l.AdditionalData1 like 'InstallType%IsCommercial' then l.AdditionalData1 end , ';')
					) l
			  PIVOT (min(Val)
				FOR Id in ([1], [2])
				) as p
			 ) s
		    for JSON AUTO
			) jisc ( dynamicIsCommericalJSON )