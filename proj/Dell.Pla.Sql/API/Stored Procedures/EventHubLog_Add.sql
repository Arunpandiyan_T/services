﻿create procedure [API].[EventHubLog_Add]
  @EventHubInfoId int
, @EventType nvarchar(255) = 'Audit'
, @Note nvarchar(max) = null
as
BEGIN
	set nocount on;

	INSERT INTO [API].[EventHubLog]
			   ([EventHubInfoId]
			   ,[When]
			   ,[EventType]
			   ,[Note])
		 VALUES
			   (@EventHubInfoId
			   ,GetUtcDate()
			   ,@EventType
			   ,@Note);
END