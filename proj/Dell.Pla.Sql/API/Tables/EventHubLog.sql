﻿CREATE TABLE [API].[EventHubLog] (
    [EventHubLogId]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [EventHubInfoId] INT            NOT NULL,
    [When]           DATETIME       DEFAULT (getutcdate()) NOT NULL,
    [EventType]      NVARCHAR (255) NOT NULL,
    [Note]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_API_EventHubLog] PRIMARY KEY CLUSTERED ([EventHubLogId] ASC)
);

