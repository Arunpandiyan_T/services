﻿CREATE TABLE [API].[EventHubInfo] (
    [EventHubInfoId]          INT             IDENTITY (1, 1) NOT NULL,
    [Namespace]               NVARCHAR (2048) NOT NULL,
    [EventHubName]            NVARCHAR (2048) NOT NULL,
    [Partitions]              INT             DEFAULT ((32)) NOT NULL,
    [Publisher]               NVARCHAR (255)  DEFAULT ('1') NOT NULL,
    [LeaseMinutes]            INT             DEFAULT ((15)) NOT NULL,
    [SendRuleName]            NVARCHAR (2048) NOT NULL,
    [SendEventHubKey]         NVARCHAR (2048) NOT NULL,
    [SendConnectionString]    NVARCHAR (MAX)  NULL,
    [ReceiveRuleName]         NVARCHAR (2048) NOT NULL,
    [ReceiveEventHubKey]      NVARCHAR (2048) NOT NULL,
    [ReceiveConnectionString] NVARCHAR (MAX)  NULL,
    [HostGroup]               INT             DEFAULT ((1)) NULL,
    [ManageConnectionString]  NVARCHAR (2048) NULL,
    CONSTRAINT [PK_API_EventHubInfo] PRIMARY KEY CLUSTERED ([EventHubInfoId] ASC)
);





