﻿CREATE TABLE [BI].dimDevice (
    [ServiceTag]		CHAR (7)     NOT NULL,
	DeviceVersion		INT NOT NULL,
    DeviceCreationDate	date NOT NULL,
    LastHeartbeatDate	date NULL,
    LastUpdateDateTime	date NOT NULL,
	IsTestDevice		bit NOT NULL,
	CountryName			nvarchar(50) NULL,
	OtherCountryName	nvarchar(50) NULL,
	UserType			varchar(50) NULL,
);

GO
CREATE CLUSTERED COLUMNSTORE INDEX [cci_factIPAddress]
    ON [BI].dimDevice;