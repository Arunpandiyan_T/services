﻿CREATE TABLE [BI].factHeartbeatMonth (
	MonthKey [datetime] NOT NULL,
	[ServiceTag] [char](7) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[AppVersion] [varchar](255) NULL,
	[LanguageCultureName] [nchar](5) NULL,
	[BiosRev] [nvarchar](20) NULL,
	[PlatformId] [int] NULL,
	[DefaultBrowserId] [int] NULL,
	[CpuId] [int] NULL,
	[OperatingSystemId] [int] NULL
) on eventMonthRangePS( MonthKey )
GO
CREATE CLUSTERED COLUMNSTORE INDEX [cci_HeartbeatMonth]
    ON [BI].[factHeartbeatMonth]
	ON [eventMonthRangePS] (MonthKey);

