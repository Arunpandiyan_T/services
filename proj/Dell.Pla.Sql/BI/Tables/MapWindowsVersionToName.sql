﻿CREATE TABLE BI.[MapWindowsVersionToName](
    [WindowsVersion] [varchar](20) NOT NULL primary key,
	[Name] nvarchar(255) NOT NULL,
)