﻿CREATE TABLE [BI].[Platform] (
    [PlatformId]    INT           IDENTITY (1, 1) NOT NULL,
    [PlatformName]  NVARCHAR (255) NOT NULL,
    [PlatformIdHex] CHAR (4)      NOT NULL,
    [CodeName]      NVARCHAR (255) NOT NULL,
    [MarketingName] NVARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([PlatformId] ASC)
);



