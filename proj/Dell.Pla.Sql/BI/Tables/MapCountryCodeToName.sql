﻿CREATE TABLE [BI].[MapCountryCodeToName] (
    [Code]       VARCHAR (4)   NOT NULL,
    [IsoCode2]   VARCHAR (2)   NOT NULL,
    [IsoCode3]   VARCHAR (3)   NULL,
    [IsoCountry] NVARCHAR (50) NULL,
    [Country]    NVARCHAR (50) NOT NULL,
    [Lat]        FLOAT (53)    NOT NULL,
    [Lon]        FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_MapCountryCodeToName_Code] PRIMARY KEY CLUSTERED ([Code] ASC)
);
