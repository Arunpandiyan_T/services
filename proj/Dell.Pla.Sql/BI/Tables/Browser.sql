﻿CREATE TABLE [BI].[Browser](
	[BrowserId] [int] IDENTITY(1,1) NOT NULL,
	  -- fields used as clean dimension attributes
	[BrowserName] nvarchar(80) NOT NULL,
	[BrowserVersion] nvarchar(80) NOT NULL,
	  -- fields to match BN, BV in the event ingestion
	BN nvarchar(80) NOT NULL,
	BV nvarchar(80) NOT NULL,
	PRIMARY KEY CLUSTERED (	BN, BV)
)
