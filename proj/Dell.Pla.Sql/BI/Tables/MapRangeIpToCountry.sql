﻿CREATE TABLE [BI].[MapRangeIpToCountry](
	[IPFrom] [bigint] NOT NULL,
	[IPTo] [bigint] NOT NULL,
	[Country2] [varchar](2) NULL,
	[Country3] [varchar](3) NULL,
	[CountryName] nvarchar(50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IPFrom] ASC,
	[IPTo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
