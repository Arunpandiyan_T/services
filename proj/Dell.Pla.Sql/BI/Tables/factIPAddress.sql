﻿CREATE TABLE [BI].[factIPAddress] (
    [ServiceTag] CHAR (7)     NOT NULL,
    [IpAddress]  VARCHAR (15) NOT NULL,
	CountryCode2 char(2)	  NOT NULL,
	CountryCode3 char(3)	  NOT NULL,
	CountryName  nvarchar(50)  NOT NULL
);

GO
CREATE CLUSTERED COLUMNSTORE INDEX [cci_factIPAddress]
    ON [BI].[factIPAddress];

