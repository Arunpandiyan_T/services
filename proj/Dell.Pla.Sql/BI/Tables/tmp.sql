﻿CREATE TABLE [BI].[tmp] (
 -- this table is the interface between spAddIpAddress and the SSIS
 -- package that is doing the lookup against the IP list
    [Ip]         BIGINT       NOT NULL,
    [ServiceTag] CHAR (7)     NOT NULL,
    [IpAddress]  VARCHAR (15) NOT NULL
);