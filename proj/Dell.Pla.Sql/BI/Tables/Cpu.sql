﻿CREATE TABLE [BI].[Cpu] (
    [CpuId]    INT           IDENTITY (1, 1) NOT NULL,
    [CpuName]  NVARCHAR (255) NOT NULL,
    [CpuClass] NVARCHAR (255) NULL,
    [CpuMaker] NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([CpuId] ASC)
);

