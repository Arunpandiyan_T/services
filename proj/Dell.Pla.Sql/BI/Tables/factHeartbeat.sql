﻿CREATE TABLE [BI].[factHeartbeat] (
    EventCreatedTime      datetime      NOT NULL,
    [ServiceTag]          CHAR (7)      NOT NULL,
    [ApplicationId]       INT           NOT NULL,
    [AppVersion]          VARCHAR (255) NULL,
    [LanguageCultureName] NCHAR (5)      NULL,
    [BiosRev]             NVARCHAR (20)  NULL,
    [PlatformId]          INT           NULL,
    [DefaultBrowserId]    INT           NULL,
    [CpuId]               INT           NULL,
    OperatingSystemId     INT           NULL,
) ON [eventMonthRangePS] ([EventCreatedTime]);

GO
CREATE CLUSTERED COLUMNSTORE INDEX [cci_Heartbeat]
    ON [BI].[factHeartbeat]
	ON [eventMonthRangePS] ([EventCreatedTime]);