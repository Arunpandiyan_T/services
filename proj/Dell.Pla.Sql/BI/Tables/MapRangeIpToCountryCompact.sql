﻿CREATE TABLE [BI].[MapRangeIpToCountryCompact](
	[IPFrom] [bigint] NOT NULL,
	[IPTo] [bigint] NOT NULL,
	[Country2] [varchar](2) NULL,
	[Country3] [varchar](3) NULL,
	[CountryName] nvarchar(50) NULL,
	[prevIpTo] [bigint] NULL,
	[prevCountry2] [varchar](2) NULL,
	[rnk] [bigint] NULL,
	[toDelete] [int] NOT NULL,
 CONSTRAINT [PK_MapRangeIpToCountryCompact] PRIMARY KEY CLUSTERED 
(
	[IPFrom] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)