﻿CREATE TABLE [BI].[MapLanguageCultureCodeToName] (
    [LanguageCultureCode] NVARCHAR (20)  NOT NULL,
    [Name]                NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_MapLanguageCultureCodeToName_LanguageCultureCode] PRIMARY KEY CLUSTERED ([LanguageCultureCode] ASC)
);
