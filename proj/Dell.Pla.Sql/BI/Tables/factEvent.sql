﻿CREATE TABLE [BI].[factEvent] (
    [ServiceTag]            CHAR (7)       NOT NULL,
    [DeviceVersion]         INT            NOT NULL,
    [EventTypeId]           INT            NOT NULL,
    [ApplicationId]         INT            NOT NULL,
    [EventDateTime]         datetime	   NOT NULL,
    [EventDateTime2]        VARCHAR (20)   NOT NULL,
    [EventCreatedTime]      datetime	   NOT NULL,
    [EventCreatedTime2]     VARCHAR (20)   NOT NULL,
    [AppVersion]            VARCHAR (255)  NULL,
    [Url]                   NVARCHAR (512)  NULL,
    [Filename]              NVARCHAR (255)  NULL,
    [FileVersion]           NVARCHAR (255)  NULL,
    [FileSize]              BIGINT         NULL,
    [ExitCode]              INT            NULL,
    [ItemName]              NVARCHAR (255)  NULL,
    [UploadedFilename]      NVARCHAR (255)  NULL,
    [AdditionalData1]       NVARCHAR (4000) NULL,
    [AdditionalData2]       NVARCHAR (4000) NULL,
    [DynamicApplicationFieldsJSON] NVARCHAR (4000)    NULL,

    [EULA]                  BIT            NULL,
    OobeCompleteDate        DATE           NULL,
    [LanguageCultureName]   NCHAR (5)       NULL,
    [BiosRev]               NVARCHAR (20)   NULL,
    [GeoId]                 INT            NULL,
    [PlatformId]            INT            NULL,
    [DefaultBrowserId]      INT            NULL,
    [CpuId]                 INT            NULL,
    [OperatingSystemId]     INT            NULL,
    [SystemRamMb]           INT            NULL,
    [HardDriveGbFree]       INT            NULL,
    [IsTestDevice]          BIT            NOT NULL,
    [DotNetVersionList]     NVARCHAR (512)  NULL,
	[DynamicDeviceFieldsJSON] nvarchar(4000) NULL,
) ON [eventMonthRangePS] ([EventCreatedTime]);

GO
CREATE CLUSTERED COLUMNSTORE INDEX [cci_EventId]
    ON [BI].[factEvent]
    ON [eventMonthRangePS] ([EventCreatedTime]);
