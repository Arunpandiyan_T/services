﻿CREATE TABLE [BI].[DimDate] (
    [ID]           INT          NOT NULL,
    [Date]         DATETIME     NOT NULL,
    [Day]          CHAR (2)     NOT NULL,
    [DaySuffix]    VARCHAR (4)  NOT NULL,
    [DayOfWeek]    VARCHAR (9)  NOT NULL,
    [DOWInMonth]   TINYINT      NOT NULL,
    [DayOfYear]    INT          NOT NULL,
    [WeekOfYear]   TINYINT      NOT NULL,
    [WeekOfMonth]  TINYINT      NOT NULL,
    [Month]        CHAR (2)     NOT NULL,
    [MonthName]    VARCHAR (9)  NOT NULL,
    [Quarter]      TINYINT      NOT NULL,
    [QuarterName]  VARCHAR (6)  NOT NULL,
    [Year]         CHAR (4)     NOT NULL,
    [StandardDate] VARCHAR (10) NULL,
    [HolidayText]  VARCHAR (50) NULL,
    CONSTRAINT [PK_dimDate] PRIMARY KEY CLUSTERED ([ID] ASC)
);

