﻿CREATE TABLE [BI].[DimTime] (
    [ID]           INT       IDENTITY (1, 1) NOT NULL,
    [Time]         CHAR (8)  NOT NULL,
    [Hour]         CHAR (2)  NOT NULL,
    [MilitaryHour] CHAR (2)  NOT NULL,
    [Minute]       CHAR (2)  NOT NULL,
    [Second]       CHAR (2)  NOT NULL,
    [AmPm]         CHAR (2)  NOT NULL,
    [StandardTime] CHAR (11) NULL,
    CONSTRAINT [PK_dimTime] PRIMARY KEY CLUSTERED ([ID] ASC)
);

