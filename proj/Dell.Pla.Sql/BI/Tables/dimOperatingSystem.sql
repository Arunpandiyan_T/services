﻿CREATE TABLE BI.dimOperatingSystem (
	[OperatingSystemId] [int] IDENTITY(1,1) NOT NULL, -- surrogate key on columnstore
	  -- message fields
	[OsType]  int NOT NULL,   --  -1 if NULL in the event
	OSVER     nvarchar (512) NOT NULL, -- '' if NULL in the event
	osother   nvarchar (255) NOT NULL, -- '' if NULL in the event
	  -- descriptive column NOT mandatory
	OSName	  nvarchar (255) NULL, -- using BI.MapOsTypeToName to convert OsType if possible, Unknown otw
	WindowName nvarchar (255) NULL, -- using BI.MapWindowsVersionToName to map if possible, case when osN.Name = 'WINNT' then 'Unknown' else 'N/A' end otw
	OtherName nvarchar (255) NULL, -- using BI.MapWindowsSkuToName to map if possible, '' otw
  primary key ([OsType] , OSVER  , osother)
)