﻿create procedure BI.spAddDimensionRecords
	-- parameters TBD: maybe # of hours to look back in daws.EventIngestion
as
begin
/*
 from bi.vwReadeventIngestion the list of lookup tables is:
  LEFT JOIN [BI].[Platform] pl on pl.[PlatformName] = ev.PN and pl.[PlatformIdHex] = ev.PID
  LEFT JOIN [BI].[Browser] bw on bw.BrowserName = ev.BN and bw.BrowserVersion = ev.BV
  LEFT JOIN BI.CPU cpu on cpu.CpuName = ev.CPU
  LEFT JOIN BI.dimOperatingSystem dimOS on dimOS.OsType = isnull(ev.OsType,-1) and dimOS.OSVER = isnull(ev.OSVER,'')
		and dimOS.osother = case when isnumeric(ev.osother) <> 1 then '' else convert(bigint,ev.osother) end

*/
BEGIN TRY

	--=======================================================
	--=============  PLATFORM ===============================
	--=======================================================
	-- 10.5 M events 0:40
	drop table if exists #pl
	select distinct ev.PN, ev.PID
	   into #pl
	   from BI.vwReadEventIngestionBase ev with (nolock)
	   LEFT JOIN BI.[Platform] pl on pl.PlatformName = ev.PN and pl.PlatformIdHex = ev.PID
	   WHERE pl.PlatformName is null
	      and ev.PN is not null
	      and ev.PID is not null -- not defined if both are not provided
--	  and ev.TS >= dateadd( day, -1, GetUtcDate() ) -- or different base on frequency of check

	INSERT INTO [BI].[Platform]
           ([PlatformName]
           ,[PlatformIdHex]
           ,[CodeName]
           ,[MarketingName])
	  select PN, PID, 'TBD', 'TBD'
	     from #pl

	--=======================================================
	--=============  BROWSER ================================
	--=======================================================
	-- 10.5 M events 2:50
	drop table if exists #br
	select distinct ev.BN, ev.BV
	   into #br
	   from BI.vwReadEventIngestionBase ev with (nolock)
	   LEFT JOIN BI.Browser b on b.BN = ev.BN and b.BV = ev.BV
	   WHERE b.BrowserName is null
	      and ev.BN is not null
	      and ev.BV is not null -- ??
--	  and ev.TS >= dateadd( day, -1, GetUtcDate() ) -- or different base on frequency of check

    INSERT INTO [BI].[Browser]
           ([BrowserName]
           ,[BrowserVersion]
           ,[BN]
           ,[BV])
	select BN, isnull(BV,''), BN, isnull(BV,'')
	   from #br
/*
 update bi.browser set BrowserName = 'Unknown'
  where BN in ('114fOÈh', '^qO%C3%88h', 'I5OÈh', '155O%C3%88h', 'UCOÈh', 'zgO%C3%88h 2', '%C2%B15O%C3%88h', 'I5O%C3%88h', '%C3%96%C5%93O%C3%88h', 'ê8OÈh', '%C3%85%C3%B7q%C3%B3', 'QQOÈh5'
    , 'z+OÈh' , '%C3%97|O%C3%88h', 'KRO%C3%88h', 'TODO: <ivOÈh>', '%@><', '~%C2%A6O%C3%88h%C3%B4%C2%B0%C3%80%C3%A5', 'QQO%EF%BF%BDh', 'zg‘OÈh', 'TPO%C3%88h8%CB%86H', '~%C2%A6O%C3%88h', 'UCO%C3%88h', 'hao123TPOÈh'
	, '%E2%80%A6%C2%A7TPO%C3%88h', 'Microsoft%C2%AE Windows%C2%AE Operating System', '114fO%C3%88h', 'QQOÈh', '2345zýOÈh', '115O%C3%88h', '~¦qóOÈh', '360%E2%80%B0hO%C3%88h', '%C3%8AZO%C3%88h', '2345Explorer'
	, '100O%C3%88h', '115OÈh', 'ÖœOÈh', 'TPO%C3%88h', '%C3%9DIO%C3%88h', '%E2%80%A6%C3%ABO%C3%88h', 'TODO: <))OÈh>', '~%C2%A6q%C3%B3O%C3%88h', 'QQO%C3%88h5', 'YYO%C3%88h', '%C3%AB%C2%A9O%C3%88h', '{%E2%80%98O%C3%88h'
	, '{%E2%80%98O%C3%88h', '360‰hOÈh', 'hao123TPO%C3%88h', '2345O%C3%88h', 'ÊZOÈh', '[[O%C3%88h', '2345z%C3%BDO%C3%88h', 'QQO%C3%88h6', '2345%E2%80%B9LO%C3%88h', '%C3%98%C2%9DO%C3%88h', '%C3%9E~O%C3%88h(FlyIe)', 'UCO%EF%BF%BDh'
	, 'QQOÈh6', '115O%C3%88h2.0', 'hao123TPO%EF%BF%BDh' , 'QQO%C3%88h', '114fO%C3%88h1.0', 'ØOÈh', '~%EF%BF%BDO%EF%BF%BDh', 'KROÈh', 'zg%E2%80%98O%C3%88h', '^qOÈh', '360%EF%BF%BDhO%EF%BF%BDh', 'YYOÈh', '%C3%AA8O%C3%88h'
	, 'TPOÈh8ˆH', '…§TPOÈh', '±5OÈh', 'ÝIOÈh', '%E2%80%98K:%C2%A9K', '2345z%EF%BF%BDO%EF%BF%BDh', 'TODO: <))O%C3%88h>', '~¦OÈhô°Àå', '2345‹LOÈh', '~¦OÈh', 'TPOÈh', '%C3%BFrO%C3%88h', 'E8O%C3%88h[e-Roam]'
	,'%C2%B9%C3%83O%C3%88h','2345%EF%BF%BDLO%EF%BF%BDh','%E2%80%A6%C2%A7O%C3%88h','2345OÈh','Microsoft%C2%AE Visual Studio%C2%AE 2010','360%E2%80%B0hLb','Å÷qó','ÿrOÈh','155OÈh','[[OÈh'
	,'Microsoft(R) Windows(R) Operating System', 'Microsoft® Windows® Operating System', 'Microsoft® Windows® Operating System', 'System operacyjny Microsoft® Windows®')

  update bi.browser set BrowserName = 'Internet Explorer'
  -- select * from bi.browser
  where BN like '%internet%explorer%' and BN <> 'Internet Explorer'

  update bi.browser set BrowserName = 'Kometa'
  -- select * from bi.browser
  where BN like '%Kometa%' and BN <> 'Kometa'

  update bi.browser set BrowserName = 'Google Chrome'
  -- select * from bi.browser
  where BN like '%Chrome%' and BN <> 'Google Chrome'

  update bi.browser set BrowserName = 'Opera Internet Browser'
  -- select * from bi.browser
  where BN like '%Opera%' and BN <> 'Opera Internet Browser' and BrowserName <> 'Unknown'

 select BrowserName, count(*)
   from bi.browser
   group by BrowserName
   order by 2 desc
*/
	--=======================================================
	--=============  CPU ====================================
	--=======================================================
	-- 10.5 M events 
	drop table if exists #cpu
	select distinct CPU
	   into #cpu
	   from BI.vwReadEventIngestionBase ev with (nolock)
	   LEFT JOIN BI.Cpu cpu on cpu.CpuName = ev.CPU
	   WHERE cpu.CpuName is null
--	  and ev.TS >= dateadd( day, -1, GetUtcDate() ) -- or different base on frequency of check

	INSERT INTO [BI].[Cpu]
			   ([CpuName]
			   ,[CpuClass]
			   ,[CpuMaker])
    select ev.CPU, 
			CpuClass = case
				when CPU like '%Intel%Core(TM)%' 	then 'Core'
				when CPU like '%Intel%Atom(TM)%' 	then 'Atom'		
				when CPU like '%Intel%Xeon(%' 		then 'Xeon'		
				when CPU like '%Intel%Pentium(R)%' 	then 'Pentium'	
				when CPU like '%Intel%Celeron(R)%' 	then 'Celeron'	
				when CPU like '%AMD% A6-%' 				then 'A6'		
				when CPU like '%AMD% A10-%' 			then 'A10'		
				when CPU like '%AMD% A8-%' 				then 'A8'		
				when CPU like '%AMD% A4-%' 				then 'A4'		
				when CPU like '%AMD% E1-%' 				then 'E1'		
				when CPU like '% Athlon(tm)%' 		then 'Athlon'	
				when CPU like '% Phenom(tm)%' 		then 'Phenom'	
				when CPU like '% Turion(tm)%' 		then 'Turion'	
				when CPU like '% Athlon(tm)%' 		then 'Athlon'	
				when CPU like '% Sempron(tm)%' 		then 'Sempron'	 
				else 'Unknown'   end,
			CpuMaker = case
				when CPU like '%Intel%' then 'Intel'
				when CPU like '%AMD%' then 'AMD'
				when CPU like '%Pentium(R)%' then 'Intel'

				when CPU like '%Celeron(R)%' then 'Intel'
				else 'Unknown'   end
	  from #cpu ev

	--=======================================================
	--=============  OPERATING SYSTEM =======================
	--=======================================================
	drop table if exists #os
	select distinct OsType, OSVER, osother = convert(bigint, osother)
	   into #os
	   from BI.vwReadEventIngestionBase with (nolock)
--	  and ev.TS >= dateadd( day, -1, GetUtcDate() ) -- or different base on frequency of check

	INSERT INTO [BI].[dimOperatingSystem]
			   ([OsType]
			   ,[OSVER]
			   ,[osother]
			   ,[OSName]
			   ,[WindowName]
			   ,[OtherName])
	select o.OsType, o.OSVER, isnull(o.osother,'') , isnull(osN.Name,'Unknown'), isnull(os.Name,case when osN.Name = 'WINNT' then 'Unknown' else 'N/A' end), isnull(WindowsSkuName,'')
		from (
			select distinct OsType = isnull(t.OsType,-1)
						   ,OSVER = isnull(t.OSVER,'')
						   ,osother = isnull(t.osother,'')
			   from #os t
			   left join BI.dimOperatingSystem dim on dim.OsType = isnull(t.OsType,-1) and dim.OSVER = isnull(t.OSVER,'') and dim.osother = isnull(t.osother,'')
			   where isnumeric(t.osother) = 1 -- this imply that OperatingSystemId is null if osother is non numeric
				and coalesce( t.OsType, t.OSVER, t.osother ) is not null
				and dim.OsType is null
			   ) o
	LEFT JOIN BI.MapWindowsVersionToName os on o.OSVER = os.WindowsVersion
	LEFT JOIN BI.[MapOsTypeToName] osN on osN.OsType = o.OsType
	LEFT JOIN BI.[MapWindowsSkuName] sku on isnumeric(o.osother) = 1 and sku.WindowsSku = convert(bigint, o.osother)

	INSERT INTO [BI].[dimOperatingSystem]
			   ([OsType]
			   ,[OSVER]
			   ,[osother]
			   ,[OSName]
			   ,[WindowName]
			   ,[OtherName])
	select o.OsType, o.OSVER, isnull(o.osother,'') , isnull(osN.Name,'Unknown'), isnull(os.Name,case when osN.Name = 'WINNT' then 'Unknown' else 'N/A' end), ''
		from (
			select distinct OsType = isnull(t.OsType,-1)
						   ,OSVER = isnull(t.OSVER,'')
						   ,osother = ''
			   from #os t
			   left join BI.dimOperatingSystem dim on dim.OsType = isnull(t.OsType,-1) and dim.OSVER = isnull(t.OSVER,'') and dim.osother = ''
			   where isnumeric(t.osother) <> 1 -- this imply that OperatingSystemId is null if osother is non numeric
			    and coalesce( t.OsType, t.OSVER ) is not null
				and dim.OsType is null
			   ) o
	LEFT JOIN BI.MapWindowsVersionToName os on o.OSVER = os.WindowsVersion
	LEFT JOIN BI.[MapOsTypeToName] osN on osN.OsType = o.OsType

END TRY
BEGIN CATCH
    THROW;
END CATCH;

end