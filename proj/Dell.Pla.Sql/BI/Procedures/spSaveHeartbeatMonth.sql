﻿
create procedure spSaveHeartbeatMonth (
		@dt date
	)
as
begin
BEGIN TRY
    if @dt is null 
		set @dt = getUtcDate()

	declare @minDt date
	declare @maxDt date

	set @minDt = dateadd( day, -1 * day( @dt ) + 1, @dt )
	set @maxDt = EOMONTH( @dt  )
	select @minDt , @maxDt

INSERT INTO [BI].[factHeartbeatMonth]
           ([MonthKey]
           ,[ServiceTag]
           ,[ApplicationId]
           ,[AppVersion]
           ,[LanguageCultureName]
           ,[BiosRev]
           ,[PlatformId]
           ,[DefaultBrowserId]
           ,[CpuId]
           ,[OperatingSystemId])
	select
			   EventCreatedTime 
			  ,ServiceTag
			  ,[ApplicationId]
			  ,[AppVersion]
			  ,[LanguageCultureName]
			  ,[BiosRev]
			  ,[PlatformId]
			  ,[DefaultBrowserId]
			  ,[CpuId]
			  ,[OperatingSystemId]
	  from (
		select ServiceTag
			  ,EventCreatedTime
			  ,[ApplicationId]
			  ,[AppVersion]
			  ,[LanguageCultureName]
			  ,[BiosRev]
			  ,[PlatformId]
			  ,[DefaultBrowserId]
			  ,[CpuId]
			  ,[OperatingSystemId]
			  ,monthRnk = row_number() over (partition by ServiceTag, ApplicationId order by EventCreatedTime desc)
		 from
		   -- with the union all the query is slower (we may have to materialize the view)
		  (	select
			   ServiceTag
			  ,EventCreatedTime
			  ,[ApplicationId]
			  ,[AppVersion]
			  ,[LanguageCultureName]
			  ,[BiosRev]
			  ,[PlatformId]
			  ,[DefaultBrowserId]
			  ,[CpuId]
			  ,[OperatingSystemId]
			  ,monthRnkInt = row_number() over (partition by ServiceTag, ApplicationId order by EventCreatedTime desc)
				from BI.factHeartbeat
				where EventCreatedTime between @minDt and @maxDt
			union all
			select
			   ServiceTag
			  ,EventCreatedTime
			  ,[ApplicationId]
			  ,[AppVersion]
			  ,[LanguageCultureName]
			  ,[BiosRev]
			  ,[PlatformId]
			  ,[DefaultBrowserId]
			  ,[CpuId]
			  ,[OperatingSystemId]
			  ,monthRnkInt = row_number() over (partition by ServiceTag, ApplicationId order by EventCreatedTime desc)
				from BI.factEvent
				where EventCreatedTime between @minDt and @maxDt
			) e
		  where monthRnkInt = 1
	  ) hb
	  where monthRnk = 1

END TRY
BEGIN CATCH
    THROW;
END CATCH;

end