﻿create procedure BI.spRebuildDimDevice
as
begin
BEGIN TRY
 truncate table [BI].[dimDevice]

-- on 4M devices: 0:50
INSERT INTO [BI].[dimDevice]
           ([ServiceTag]
           ,[DeviceVersion]
           ,[DeviceCreationDate]
           ,[LastHeartbeatDate]
           ,[LastUpdateDateTime]
           ,[IsTestDevice]
           ,[CountryName]
           ,[OtherCountryName]
           ,[UserType])
SELECT d.ServiceTag
      ,d.DeviceVersion
      ,convert(date, d.DeviceCreatedDateTime)
	  ,hb.LastHeartbeatDate
      ,d.LastUpdateDateTime
      ,d.IsTestDevice
	  ,ip.Country1
	  ,ip.Country2
	  ,ip.UserType
  FROM [DAWS].[DeviceIngestion] d
  left join (select ServiceTag, LastHeartbeatDate = Max(LastHeartbeatDate) from 
                   (select ServiceTag, LastHeartbeatDate = Max([EventCreatedTime]) 
				       from BI.factHeartbeat 
					   group by ServiceTag
					union all
					select ServiceTag, LastHeartbeatDate = Max([EventCreatedTime])
						from BI.factEvent
						group by ServiceTag
					) ev
				 group by ServiceTag
				) hb on d.ServiceTag = hb.ServiceTag
  left join (select ServiceTag
				  , Country1 = Min(CountryName)
				  , Country2 = case when Min(CountryName) <> Max(CountryName) then Max(CountryName) end
				  , UserType = case when count(distinct IpAddress) > 3 then 'Business User' else 'Home user' end
				   from BI.factIPAddress
				   group by ServiceTag
				   ) ip on ip.ServiceTag = d.ServiceTag

-- select top 10000 * from bi.dimDevice where othercountryname is not null

END TRY
BEGIN CATCH
    THROW;
END CATCH;

end