﻿create procedure BI.spAddPreviousDayToFacts
as
begin
BEGIN TRY

-- truncate table [BI].[factEvent]
-- truncate table [BI].[factHeartbeat]
  declare @prevDay date = dateadd(day, -1, GetUtcDate())

	INSERT INTO [BI].[factHeartbeat]
			   (EventCreatedTime
			   ,[ServiceTag]
			   ,[ApplicationId]
			   ,[AppVersion]
			   ,[LanguageCultureName]
			   ,[BiosRev]
			   ,[PlatformId]
			   ,[DefaultBrowserId]
			   ,[CpuId]
			   ,OperatingSystemId )
	SELECT
		   EventCreatedTime
		  ,[ServiceTag]
		  ,[ApplicationId]
		  ,[AppVersion]
		  ,[LanguageCultureName]
		  ,[BiosRev]
		  ,PlatformId
		  ,DefaultBrowserId
		  ,CpuId
		  ,OperatingSystemId
	  FROM BI.vwReadEventIngestion
	  where EventTypeId = 7  -- we could save in this table also OneTimePing, ButtonClicked and maybe other event types
	   and convert(date, EventCreatedTime) = @prevDay

	INSERT INTO [BI].[factEvent]
			   (
				[DeviceVersion]
			   ,[ServiceTag]
			   ,[EventTypeId]
			   ,[ApplicationId]
			   ,EventDateTime
			   ,[EventDateTime2]
			   ,EventCreatedTime
			   ,[EventCreatedTime2]
			   ,[AppVersion]
			   ,[Url]
			   ,[Filename]

			   ,[FileVersion]
			   ,[FileSize]
			   ,[ExitCode]
			   ,[ItemName]
			   ,[UploadedFilename]
			   ,[AdditionalData1]
			   ,[AdditionalData2]
			   ,EULA
			   ,OobeCompleteDate
			   ,[LanguageCultureName]
			   ,[BiosRev]
			   ,[GeoId]
			   ,[PlatformId]
			   ,[DefaultBrowserId]
			   ,[CpuId]
			   ,[OperatingSystemId]
			   ,[SystemRamMb]
			   ,[HardDriveGbFree]
			   ,[IsTestDevice]
			   ,[DotNetVersionList]
			   ,[DynamicApplicationFieldsJSON]
			   ,[DynamicDeviceFieldsJSON]
		)
	SELECT
		[DeviceVersion]
		,[ServiceTag]
		,[EventTypeId]
		,[ApplicationId]
		,EventDateTime
		,EventDateTime2
		,EventCreatedTime
		,EventCreatedTime2
		,[AppVersion]
		,[Url]
		,[Filename]
		,[FileVersion]
		,[FileSize]
		,[ExitCode]
		,[ItemName]
		,[UploadedFilename]
		,[AdditionalData1]
		,[AdditionalData2]
		,EULA
		,OobeCompleteDate
		,[LanguageCultureName]
		,[BiosRev]
		,[GeoId]
		,PlatformId
		,DefaultBrowserId
		,CpuId
		,OperatingSystemId
		,SystemRamMb
		,HardDriveGbFree
		,[IsTestDevice]
		,DotNetVersionList
		,[DynamicApplicationFieldsJSON]
		,[DynamicDeviceFieldsJSON]
	  FROM BI.vwReadEventIngestion
	  where EventTypeId <> 7
	   and convert(date, EventCreatedTime) = @prevDay

END TRY
BEGIN CATCH
    THROW;
END CATCH;

end