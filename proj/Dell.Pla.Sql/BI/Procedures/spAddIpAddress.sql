﻿create procedure BI.spAddIPAddress
	-- parameters TBD: maybe # of hours to look back in daws.EventIngestion
as
begin
BEGIN TRY
     -- read the list of IP arrived since the last check
	drop table if exists #NewIPs
	create table #NewIPs (
	 ServiceTag char(7) not null,
	 IpAddress varchar(15) not null,
	 Ip bigint not null,
	 primary key (ServiceTag, IpAddress)
	 )

	insert into #NewIPs (ServiceTag, IpAddress, Ip)
      select distinct ST, IpAddress,
			IP = CAST( PARSENAME( IpAddress, 4 ) AS bigint ) * 16777216 + CAST( PARSENAME( IpAddress, 3 ) AS bigint ) * 65536 +
			          CAST( PARSENAME( IpAddress, 2 ) AS bigint ) * 255 + CAST( PARSENAME( IpAddress, 1 ) AS bigint )
		    	  from BI.vwReadEventIngestionBase ev
	  where isnumeric( replace(ev.IpAddress, '.','') ) = 1 -- just to make sure...
--	    and ev.TS >= dateadd( day, -1, getUtcDate() ) -- or different base on frequency of check
	-- on 10.5M Events --> 2:28
	-- the rest of the script is "safe", because it is using #temp and bi.factAddress (we can use a temp table to avoid locking the table for too long)

     -- evaluate the list of IP arrived since the last check that needs to be inserted
	-- 3,897,079 first load
	-- 1,172,664 for last day
	drop table if exists BI.tmp
	create table BI.tmp (
	 Ip bigint not null,
	 ServiceTag char(7) not null,
	 IpAddress varchar(15) not null,
	 primary key (Ip, ServiceTag)
	 )

	insert into BI.tmp (ServiceTag, IpAddress, Ip )
	 select distinct ev.ServiceTag, ev.IpAddress, ev.Ip
	  from #NewIPs ev
	  left join BI.factIPAddress ip on ip.ServiceTag = ev.ServiceTag and ip.IpAddress = ev.IpAddress
	  where ip.ServiceTag is null
	--create index idx_IpTemp on #temp ( ip )

	-- after this procedure the SSIS package will load bi.tmp into bi.factIpAddress
	-- the time required is about 2 minutes/M rows

END TRY
BEGIN CATCH
    THROW;
END CATCH;

end