﻿create procedure BI.spCompactIpRange
as
begin
/*
 drop contigous records in the MapRangeIpToCountry (the database is more detailed than needed)
 to try to speed up the lookup
*/
BEGIN TRY
	truncate table BI.MapRangeIpToCountryCompact
	ALTER TABLE BI.MapRangeIpToCountryCompact DROP CONSTRAINT IF EXISTS PK_MapRangeIpToCountryCompact

	insert into BI.MapRangeIpToCountryCompact
	select *
	 from (
		select *, toDelete = case when IPFrom = prevIpTo and Country2 = PrevCountry2 then 1 else 0 end
		 from (
			select *,
					 prevIpTo = lag(IPTo,1) over( order by IPFrom)+1, prevCountry2 = lag(Country2,1) over( order by IPFrom),
					 rnk = row_number() over( order by IPFrom )
			 from [BI].[MapRangeIpToCountry]
		  ) l
		) l1

	DROP TABLE IF EXISTS #d
	CREATE TABLE #d (
		[IPFrom] [bigint] NOT NULL,
		[IPTo] [bigint] NOT NULL,
		[Country2] [varchar](2) NULL,
		[Country3] [varchar](3) NULL,
		[CountryName] [varchar](50) NULL,
		[prevIpTo] [bigint] NULL,
		[prevCountry2] [varchar](2) NULL,
		[rnk] [bigint] NULL,
		[toDelete] [int] NOT NULL
	)

	declare @step int = 2
	declare @rows int = 1

	while @rows > 0
	begin
		truncate table #d
		insert into #d
		select d.*
		  from BI.MapRangeIpToCountryCompact k
		  join BI.MapRangeIpToCountryCompact d on k.toDelete = 0 and d.toDelete = 1 and k.rnk + @step = d.rnk and k.IPTo+1 = d.IPFrom

		update k set k.IPTo = d.IPTo
		-- select *
		  from BI.MapRangeIpToCountryCompact k
		  join #d d on k.toDelete = 0 and d.toDelete = 1 and k.rnk +@step = d.rnk

		delete from BI.MapRangeIpToCountryCompact 
		 where rnk in (select rnk from #d)

		set @rows = @@ROWCOUNT
		set @step = @step + 1
	end

	-- select count(*) from BI.MapRangeIpToContryCompact

	ALTER TABLE BI.MapRangeIpToCountryCompact ADD CONSTRAINT PK_MapRangeIpToCountryCompact  PRIMARY KEY CLUSTERED (	[IPFrom] )

END TRY
BEGIN CATCH
    THROW;
END CATCH;

end