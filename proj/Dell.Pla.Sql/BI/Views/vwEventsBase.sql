﻿CREATE view [BI].[vwEventsBase]
as
 SELECT
       [ServiceTag]
      ,[EventTypeId]
      ,[ApplicationId]
      ,[EventDateTime]
      ,[EventCreatedDate]
      ,[EventCreatedTime]
	  ,[EventCreatedDateTime] = convert(datetime2, convert(varchar(20),[EventCreatedDate]) +' '+ convert(varchar(20),[EventCreatedTime]) )
      ,[AppVersion]
      ,[Url]
      ,[Filename]
      ,[DeviceInfoJSON]
      --PlatformName = JSON_VALUE( [DeviceInfoJSON], '$.PN' )
      --,IPAddress = JSON_VALUE( [DeviceInfoJSON], '$.IpAddress' )
      --,PeaVal = JSON_VALUE( [DeviceInfoJSON], '$.PeaVal' )
      --,LanguageCultureName = JSON_VALUE( [DeviceInfoJSON], '$.LC' )
      --,BiosRev = JSON_VALUE( [DeviceInfoJSON], '$.BR' )
      --,OsVersion = JSON_VALUE( [DeviceInfoJSON], '$.OSVER' )
      --,OsType = JSON_VALUE( [DeviceInfoJSON], '$.OsType' )
      --,BrowserName = JSON_VALUE( [DeviceInfoJSON], '$.BN' )
      --,BrowserVersion = JSON_VALUE( [DeviceInfoJSON], '$.BV' )
      --,GeoId = JSON_VALUE( [DeviceInfoJSON], '$.GeoId' )
      --,SystemRamMb = JSON_VALUE( [DeviceInfoJSON], '$.M' )
      ,[DeviceRegistrationInfoJSON]
      --,OobeCompleteDate = JSON_VALUE( [DeviceRegistrationInfoJSON], '$.OCD' )
      --,HardDriveGbFree  = JSON_VALUE( [DeviceRegistrationInfoJSON], '$.GbF' )
      --,Cpu = JSON_VALUE( [DeviceRegistrationInfoJSON], '$.CPU' )
      --,OsOtherId = JSON_VALUE( [DeviceRegistrationInfoJSON], '$.osother' )
      ,[DotNetVersionList]
      ,[ApplicationFieldsListJSON]
      --,FileVersion  = JSON_VALUE( [ApplicationFieldsListJSON], '$.FileVersion' )
      --,FileSize = JSON_VALUE( [ApplicationFieldsListJSON], '$.FileSize' )
      --,ExitCode = JSON_VALUE( [ApplicationFieldsListJSON], '$.ExitCode' )
      --,ItemName = JSON_VALUE( [ApplicationFieldsListJSON], '$.ItemName' )
      --,UploadedFileName = JSON_VALUE( [ApplicationFieldsListJSON], '$.UploadedFileName' )
      --,EventTrackingCode = JSON_VALUE( [ApplicationFieldsListJSON], '$.EventTrackingCode' )
      ,[DynamicFieldsListJSON]
      --,AdditionalData1 = JSON_VALUE( [DynamicFieldsListJSON], '$.Add1' )
      --,AdditionalData2 = JSON_VALUE( [DynamicFieldsListJSON], '$.Add2' )
  FROM [DAWS].[Event] with (nolock)