﻿CREATE VIEW BI.vwReadEventIngestionBase
as
SELECT
	TS
	,ev.[DeviceVersion]
	,ev.ST
	,[EventTypeId]
	,[ApplicationId]
	,EventDateTime
	,[AppVersion]
	,[Url]
	,[Filename]
	,[FileVersion]
	,[FileSize]
	,[ExitCode]
	,[ItemName] =
		case when EventTypeId in (5,6, 29,31,1,2,3) then upper(substring([AdditionalData1],1,1)) + 'AX01 description' else [ItemName] end
	,[UploadedFilename]
	,[AdditionalData1] =
		case when EventTypeId in (5,6, 29,31,1,2,3) then upper(substring([AdditionalData1],1,1)) + 'AX01' else [AdditionalData1] end
	,[AdditionalData2]
	,[DynamicApplicationFieldsJSON]

	,EULA
	,OCD
	,LC
	,BR
	,[GeoId]
	,M
	,[GbF]
	,[IsTestDevice]
	,DNV
	,[DynamicDeviceFieldsJSON]

	,PN = case substring(ST,7,1)
			when '0' then 'Inspiron 11 - 3147'
			when '1' then 'Inspiron 20 Model 3043'
			when '2' then 'Inspiron 5558'
			when '3' then 'Latitude E5420'
			when '4' then 'Latitude E7440'
			when '5' then 'Inspiron 2350'
			when '6' then 'Dimension 4600'
			when '7' then 'Dell System Vostro 3750'
			when '8' then 'Dell XPS710'
			else 'Alienware 18' end
	,PID =	case substring(ST,7,1)
			when '0' then '064D'
			when '1' then '0690'
			when '2' then '06AE'
			when '3' then '049B'
			when '4' then '05CB'
			when '5' then '05DB'
			when '6' then '0155'
			when '7' then '04DA'
			when '8' then '0207'
			else '05AB' end
	,BN = 'Google Chrome'
	,BV = 	case 
			when substring(BV,1,1) between 'a' and 'f' then '15.0.849.0'
			when substring(BV,1,1) between 'g' and 'l' then '15.0.874.102'
			when substring(BV,1,1) between 'm' and 'q' then '17.0.942.0'
			when substring(BV,1,1) between 'r' and 'v' then '15.0.874.106'
			else '15.0.874.121' end
	,CPU =	case substring(ST,7,1)
			when '0' then 'A10-6800K'
			when '1' then 'AMD A4-1350 APU with Radeon(TM) HD Graphics'
			when '2' then 'AMD A6-1450 APU with Radeon(TM) HD Graphics'
			when '3' then 'AMD A8-3500M APU with Radeon(tm) HD Graphics'
			when '4' then 'Intel(R) Atom(TM) CPU  230   @ 1.60GHz'
			when '5' then 'Celeron(R) Dual-Core CPU       T3000  @ 1.80GHz'
			when '6' then 'Core i3-6010U CPU @ 3.0GHz'
			when '7' then 'Intel Pentium III family 1400MHz'
			when '8' then 'Genuine Intel(R) CPU                  @ 2.16GHz'
			else 'Intel(R) Xeon(R) CPU            3040  @ 1.86GHz' end
	,x.OsType
	,x.OSVER
	,x.osother
	  -- other fields not used in vwReadEventIngestion
	,IpAddress
	,EventTrackingCode
  FROM [DAWS].EventIngestion ev with (nolock)
  left join (values
		(18,'10.0.10240',100,1,1),
		(18,'10.0.10240',101,2,1),
		(18,'10.0.10240',121,3,1),
		(18,'10.0.10240',125,4,1),
		(18,'10.0.10240',129,5,1),
		(18,'10.0.10240',27,6,1),
		(18,'10.0.10240',4,7,1),
		(18,'10.0.10240',48,8,1),
		(18,'10.0.10240',49,9,1),
		(18,'10.0.10240',99,0,1),
		(18,'10.0.10240',122,1,2),
		(18,'10.0.10240',72,2,2),
		(18,'10.0.9888',48,3,2),
		(18,'6.1.7601',7,4,2),
		(18,'6.1.7601',8,5,2),
		(18,'6.1.7601',33,6,2),
		(18,'6.2.9200',98,7,2),
		(18,'6.2.9200',99,8,2),
		(18,'6.2.9200',8,9,2),
		(18,'6.2.8400',48,0,2),
		(18,'6.1.7600',1,1,3),
		(18,'6.1.7600',11,2,3),
		(18,'6.1.7600',2,3,3),
		(18,'6.1.7600',3,4,3),
		(18,'6.1.7600',4,5,3),
		(18,'6.1.7600',48,6,3),
		(18,'6.1.7600',7,7,3),
		(18,'6.1.7601',48,8,3),
		(18,'6.1.7601',49,9,3),
		(18,'6.1.7601',65,0,3),
		(18,'6.1.7601',null,1,4),
		(18,'6.1.7601',1,2,4),
		(18,'6.1.7601',10,3,4),
		(18,'6.1.7601',11,4,4),
		(18,'6.1.7601',2,5,4),
		(18,'6.1.7601',26,6,4),
		(18,'6.1.7601',27,7,4),
		(18,'6.1.7601',28,8,4),
		(18,'6.1.7601',3,9,4),
		(18,'6.1.7601',4,0,4),
		(18,'6.2.9200',100,1,5),
		(18,'6.2.9200',101,2,5),
		(18,'6.2.9200',103,3,5),
		(18,'6.2.9200',27,4,5),
		(18,'6.2.9200',2882382797,5,5),
		(18,'6.2.9200',4,6,5),
		(18,'6.2.9200',48,7,5),
		(18,'6.2.9200',49,8,5),
		(18,'6.2.9200',7,9,5),
		(18,'6.2.9200',72,0,5),
		(18,'6.0.6002',2,1,6),
		(18,'6.0.6002',2,2,6),
		(18,'6.0.6002',3,3,6),
		(18,'6.0.6002',3,4,6),
		(18,'6.0.6002',6,5,6),
		(18,'6.0.6002',6,6,6),
		(18,'6.0.6002',7,7,6),
		(18,'6.0.6002',7,8,6),
		(18,'6.0.6002',1,9,6),
		(18,'6.0.6002',1,0,6),
		(18,'6.3.9431',48,1,7),
		(18,'6.3.9600',7,2,7),
		(18,'6.3.9600',72,3,7),
		(18,'6.3.9600',8,4,7),
		(18,'6.3.9600',89,5,7),
		(18,'6.3.9600',91,6,7),
		(18,'6.3.9600',98,7,7),
		(18,'6.3.9600',99,8,7),
		(18,'6.0.6000',6,9,7),
		(18,'6.0.6000',3,0,7),
		(18,'6.3.9600',null,1,8),
		(18,'6.3.9600',100,2,8),
		(18,'6.3.9600',101,3,8),
		(18,'6.3.9600',103,4,8),
		(18,'6.3.9600',27,5,8),
		(18,'6.3.9600',33,6,8),
		(18,'6.3.9600',4,7,8),
		(18,'6.3.9600',48,8,8),
		(18,'6.3.9600',49,9,8),
		(18,'6.3.9600',50,0,8),
		(18,'10.0.10240',100,0,9),
		(18,'10.0.10240',101,1,9),
		(18,'10.0.10240',122,2,9),
		(18,'10.0.10240',72,3,9),
		(18,'6.1.7600',1,4,9),
		(18,'6.1.7600',11,5,9),
		(18,'6.1.7601',null,6,9),
		(18,'6.1.7601',1,7,9),
		(18,'6.2.9200',100,8,9),
		(18,'6.2.9200',101,9,9),
		(18,'6.2.9200',103,0,0),
		(18,'6.2.9200',27,1,0),
		(18,'6.0.6002',2,2,0),
		(18,'6.0.6002',2,3,0),
		(18,'6.3.9431',48,4,0),
		(18,'6.3.9600',7,5,0),
		(18,'6.3.9600',72,6,0),
		(18,'6.3.9600',null,7,0),
		(18,'6.3.9600',100,8,0),
		(18,'6.3.9600',101,9,0) ) x (OsType	, OSVER, osother ,rnkOther, rnkOsType)
			on x.rnkOther = ev.osother and x.rnkOsType = ev.OsType