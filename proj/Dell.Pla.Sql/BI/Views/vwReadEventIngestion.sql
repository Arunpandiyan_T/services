﻿create view BI.vwReadEventIngestion
as
SELECT
	TS as [EventCreatedDate]
	,ev.[DeviceVersion]
	,ev.ST as [ServiceTag]
	,[EventTypeId]
	,[ApplicationId]
	,EventDateTime = convert(datetime, substring( convert(varchar(30), EventDateTime), 1, 16))
	,EventDateTime2 = substring( convert(varchar(30), EventDateTime), 18, 30)
	,EventCreatedTime = convert(datetime, substring( convert(varchar(30), TS), 1, 16))
	,EventCreatedTime2 = substring( convert(varchar(30), TS), 18, 30)
	,[AppVersion]
	,[Url] = case  when ev.Url like '%?AuthGUID=%' then substring( ev.Url, 1, patindex('%?AuthGUID=%', ev.Url)+8) else ev.Url end
	,[Filename]
	,[FileVersion]
	,[FileSize]
	,[ExitCode]
	,[ItemName]
	,[UploadedFilename]
	,[AdditionalData1]
	,[AdditionalData2]
	,[DynamicApplicationFieldsJSON]

	,EULA
	,[OCD] as OobeCompleteDate
	,LC as [LanguageCultureName]
	,BR as [BiosRev]
	,[GeoId]
	,PlatformId = isnull(pl.PlatformId,-1)
	,DefaultBrowserId = isnull(bw.BrowserId,-1)
	,CpuId = isnull(cpu.CpuId, -1)
	,OperatingSystemId = isnull(dimOS.OperatingSystemId,-1)
	,SystemRamMb = M
	,HardDriveGbFree = [GbF]
	,[IsTestDevice]
	,DotNetVersionList = ev.DNV
	,[DynamicDeviceFieldsJSON]
  FROM BI.vwReadEventIngestionBase ev with (nolock)
  LEFT JOIN [BI].[Platform] pl on pl.[PlatformName] = ev.PN and pl.[PlatformIdHex] = ev.PID
  LEFT JOIN [BI].[Browser] bw on bw.BrowserName = ev.BN and bw.BrowserVersion = ev.BV
  LEFT JOIN BI.Cpu cpu on cpu.CpuName = ev.CPU
  LEFT JOIN BI.dimOperatingSystem dimOS on dimOS.OsType = isnull(ev.OsType,-1) and dimOS.OSVER = isnull(ev.OSVER,'')
		and dimOS.osother = case when isnumeric(ev.osother) <> 1 then '' else convert(bigint,ev.osother) end