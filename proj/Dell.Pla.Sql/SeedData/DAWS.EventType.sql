﻿IF not EXISTS (SELECT  * from [Daws].[EventType])
BEGIN
SET IDENTITY_INSERT [Daws].[EventType] ON
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (1,'DownloadStarted','The source started downloading a file');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (2,'DownloadCompleted','The source finished downloading a file');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (3,'DownloadFailed','The source failed to download a file');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (4,'InstallationStarted','The source started installing a file');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (5,'InstallationCompleted','The source finished installing a file');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (6,'InstallationFailed','The source finished installing a file');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (7,'ApplicationHeartbeat','The source is running');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (8,'ApplicationStarted','The source application has started');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (9,'ApplicationExited','The source application has exited');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (10,'ApplicationCrashed','The source application crashed');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (11,'SystemShutdown','The system is shutting down');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (12,'SystemReboot','The system is rebooting');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (13,'SystemSleep','The system is going to sleep');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (14,'SystemHibernate','The system is hibernating');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (15,'SystemResume','The system is resuming');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (16,'BandwidthTested','The application tested the machine''s bandwidth');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (17,'DeviceInfoChanged','Certain device information has been updated');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (18,'ClientReset','The client has been reset');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (19,'ButtonClicked','Some button was clicked');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (20,'NavigationAction','User navigated to');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (21,'ClickAction','User clicked on');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (22,'LoginAction','User login');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (23,'NotificationAction','User notification');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (24,'CustomEvent','Custom event');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (25,'OneTimePing','The device conducted a one-time-only Ping');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (26,'MenuItemClicked','Some menu item was clicked');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (27,'YellowBangFound','A yellow bang was found in the device manager.');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (28,'YellowBangResolved','A yellow bang that was previously found in the device manager is no longer present.');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (29,'BiosInstallationStarted','The source started installing a BIOS Update');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (30,'BiosInstallationCompleted','The source finished installing a BIOS Update');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (31,'BiosInstallationFailed','The source finished installing a BIOS Update');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (32,'PlatformCatalogNameChanged','The source changed the platform catalog name that it uses.');
insert into [Daws].[EventType] ([EventTypeId],[EventTypeName],[Description]) values (33,'InstallationSkipped','The source skipped installation of a file');
SET IDENTITY_INSERT [Daws].[EventType] OFF
END
