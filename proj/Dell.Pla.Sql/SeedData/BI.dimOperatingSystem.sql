IF not EXISTS (SELECT  * from [BI].[dimOperatingSystem])
BEGIN
SET IDENTITY_INSERT [BI].[dimOperatingSystem] ON
insert into [BI].[dimOperatingSystem] ([OperatingSystemId],[OsType],[OSVER],[osother],[OSName],[WindowName],[OtherName])
 values 
	(1,-1,'','101','Unknown','N/A','Windows 8'),
	(2,18,'10.0.10041','101','WINNT','Unknown','Windows 8'),
	(3,18,'10.0.10041','48','WINNT','Unknown','Professional'),
	(4,18,'10.0.10074','101','WINNT','Unknown','Windows 8'),
	(5,18,'10.0.10074','48','WINNT','Unknown','Professional'),
	(6,18,'10.0.10130','100','WINNT','Unknown','Windows 8 Single Language'),
	(7,18,'10.0.10130','101','WINNT','Unknown','Windows 8'),
	(8,18,'10.0.10130','48','WINNT','Unknown','Professional'),
	(9,18,'10.0.10144','48','WINNT','Unknown','Professional'),
	(10,18,'10.0.10158','48','WINNT','Unknown','Professional'),
	(11,18,'10.0.10159','48','WINNT','Unknown','Professional'),
	(12,18,'10.0.10162','101','WINNT','Unknown','Windows 8'),
	(13,18,'10.0.10162','48','WINNT','Unknown','Professional'),
	(14,18,'10.0.10163','48','WINNT','Unknown','Professional'),
	(15,18,'10.0.10166','48','WINNT','Unknown','Professional'),
	(16,18,'10.0.10240','100','WINNT','Windows 10 RTM','Windows 8 Single Language'),
	(17,18,'10.0.10240','101','WINNT','Windows 10 RTM','Windows 8'),
	(18,18,'10.0.10240','121','WINNT','Windows 10 RTM',''),
	(19,18,'10.0.10240','125','WINNT','Windows 10 RTM',''),
	(20,18,'10.0.10240','129','WINNT','Windows 10 RTM',''),
	(21,18,'10.0.10240','27','WINNT','Windows 10 RTM','Enterprise N'),
	(22,18,'10.0.10240','4','WINNT','Windows 10 RTM','Enterprise'),
	(23,18,'10.0.10240','48','WINNT','Windows 10 RTM','Professional'),
	(24,18,'10.0.10240','49','WINNT','Windows 10 RTM','Professional N'),
	(25,18,'10.0.10240','99','WINNT','Windows 10 RTM','Windows 8 China'),
	(26,18,'10.0.10525','100','WINNT','Unknown','Windows 8 Single Language'),
	(27,18,'10.0.10525','101','WINNT','Unknown','Windows 8'),
	(28,18,'10.0.10525','121','WINNT','Unknown',''),
	(29,18,'10.0.10525','48','WINNT','Unknown','Professional'),
	(30,18,'10.0.10532','100','WINNT','Unknown','Windows 8 Single Language'),
	(31,18,'10.0.10532','101','WINNT','Unknown','Windows 8'),
	(32,18,'10.0.10532','4','WINNT','Unknown','Enterprise'),
	(33,18,'10.0.10532','48','WINNT','Unknown','Professional'),
	(34,18,'10.0.10537','101','WINNT','Unknown','Windows 8'),
	(35,18,'10.0.10537','48','WINNT','Unknown','Professional'),
	(36,18,'10.0.10546','48','WINNT','Unknown','Professional'),
	(37,18,'10.0.10547','100','WINNT','Unknown','Windows 8 Single Language'),
	(38,18,'10.0.10547','101','WINNT','Unknown','Windows 8'),
	(39,18,'10.0.10547','4','WINNT','Unknown','Enterprise'),
	(40,18,'10.0.10547','48','WINNT','Unknown','Professional'),
	(41,18,'10.0.10550','48','WINNT','Unknown','Professional'),
	(42,18,'10.0.9888','48','WINNT','Windows 10 Technical Preview (10.0.9888)','Professional'),
	(43,18,'10.0.9926','101','WINNT','Unknown','Windows 8'),
	(44,18,'10.0.9926','48','WINNT','Unknown','Professional'),
	(45,18,'6.0.6001','3','WINNT','Windows Vista with Service Pack 1 or Windows Server 2008','Home Premium'),
	(46,18,'6.0.6002','2','WINNT','Windows Vista with Service Pack 2','Home Basic'),
	(47,18,'6.0.6002','3','WINNT','Windows Vista with Service Pack 2','Home Premium'),
	(48,18,'6.0.6002','6','WINNT','Windows Vista with Service Pack 2','Business'),
	(49,18,'6.0.6002','7','WINNT','Windows Vista with Service Pack 2','Server Standard'),
	(50,18,'6.1.7600','1','WINNT','Windows 7 or Windows Server 2008 R2','Ultimate'),
	(51,18,'6.1.7600','11','WINNT','Windows 7 or Windows Server 2008 R2','Starter'),
	(52,18,'6.1.7600','2','WINNT','Windows 7 or Windows Server 2008 R2','Home Basic'),
	(53,18,'6.1.7600','3','WINNT','Windows 7 or Windows Server 2008 R2','Home Premium'),
	(54,18,'6.1.7600','4','WINNT','Windows 7 or Windows Server 2008 R2','Enterprise'),
	(55,18,'6.1.7600','48','WINNT','Windows 7 or Windows Server 2008 R2','Professional'),
	(56,18,'6.1.7600','7','WINNT','Windows 7 or Windows Server 2008 R2','Server Standard'),
	(57,18,'6.1.7601','','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1',''),
	(58,18,'6.1.7601','1','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Ultimate'),
	(59,18,'6.1.7601','10','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Server Enterprise (full installation)'),
	(60,18,'6.1.7601','11','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Starter'),
	(61,18,'6.1.7601','2','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Home Basic'),
	(62,18,'6.1.7601','26','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Home Premium N'),
	(63,18,'6.1.7601','27','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Enterprise N'),
	(64,18,'6.1.7601','28','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Ultimate N'),
	(65,18,'6.1.7601','3','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Home Premium'),
	(66,18,'6.1.7601','4','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Enterprise'),
	(67,18,'6.1.7601','48','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Professional'),
	(68,18,'6.1.7601','49','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Professional N'),
	(69,18,'6.1.7601','65','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1',''),
	(70,18,'6.1.7601','7','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Server Standard'),
	(71,18,'6.2.9200','100','WINNT','Windows 8 or Windows Server 2012','Windows 8 Single Language'),
	(72,18,'6.2.9200','101','WINNT','Windows 8 or Windows Server 2012','Windows 8'),
	(73,18,'6.2.9200','103','WINNT','Windows 8 or Windows Server 2012','Professional with Media Center'),
	(74,18,'6.2.9200','27','WINNT','Windows 8 or Windows Server 2012','Enterprise N'),
	(75,18,'6.2.9200','2882382797','WINNT','Windows 8 or Windows Server 2012',''),
	(76,18,'6.2.9200','4','WINNT','Windows 8 or Windows Server 2012','Enterprise'),
	(77,18,'6.2.9200','48','WINNT','Windows 8 or Windows Server 2012','Professional'),
	(78,18,'6.2.9200','49','WINNT','Windows 8 or Windows Server 2012','Professional N'),
	(79,18,'6.2.9200','7','WINNT','Windows 8 or Windows Server 2012','Server Standard'),
	(80,18,'6.2.9200','72','WINNT','Windows 8 or Windows Server 2012','Server Enterprise (evaluation installation)'),
	(81,18,'6.2.9200','98','WINNT','Windows 8 or Windows Server 2012','Windows 8 N'),
	(82,18,'6.2.9200','99','WINNT','Windows 8 or Windows Server 2012','Windows 8 China'),
	(83,18,'6.3.9431','48','WINNT','Windows 8.1 Preview','Professional'),
	(84,18,'6.3.9456','101','WINNT','Unknown','Windows 8'),
	(85,18,'6.3.9600','','WINNT','Windows 8.1 with Update 1',''),
	(86,18,'6.3.9600','100','WINNT','Windows 8.1 with Update 1','Windows 8 Single Language'),
	(87,18,'6.3.9600','101','WINNT','Windows 8.1 with Update 1','Windows 8'),
	(88,18,'6.3.9600','103','WINNT','Windows 8.1 with Update 1','Professional with Media Center'),
	(89,18,'6.3.9600','27','WINNT','Windows 8.1 with Update 1','Enterprise N'),
	(90,18,'6.3.9600','33','WINNT','Windows 8.1 with Update 1','Server Foundation'),
	(91,18,'6.3.9600','4','WINNT','Windows 8.1 with Update 1','Enterprise'),
	(92,18,'6.3.9600','48','WINNT','Windows 8.1 with Update 1','Professional'),
	(93,18,'6.3.9600','49','WINNT','Windows 8.1 with Update 1','Professional N'),
	(94,18,'6.3.9600','50','WINNT','Windows 8.1 with Update 1','Windows Small Business Server 2011 Essentials'),
	(95,18,'6.3.9600','7','WINNT','Windows 8.1 with Update 1','Server Standard'),
	(96,18,'6.3.9600','72','WINNT','Windows 8.1 with Update 1','Server Enterprise (evaluation installation)'),
	(97,18,'6.3.9600','8','WINNT','Windows 8.1 with Update 1','Server Datacenter (full installation)'),
	(98,18,'6.3.9600','89','WINNT','Windows 8.1 with Update 1',''),
	(99,18,'6.3.9600','91','WINNT','Windows 8.1 with Update 1',''),
	(100,18,'6.3.9600','98','WINNT','Windows 8.1 with Update 1','Windows 8 N'),
	(101,18,'6.3.9600','99','WINNT','Windows 8.1 with Update 1','Windows 8 China'),
	(102,36,'14.04','','LINUX','N/A',''),
	(103,36,'14.04.3','','LINUX','N/A',''),
	(104,18,'10.0.10049','101','WINNT','Unknown','Windows 8'),
	(105,18,'10.0.10125','48','WINNT','Unknown','Professional'),
	(106,18,'10.0.10240','122','WINNT','Windows 10 RTM',''),
	(107,18,'10.0.10240','72','WINNT','Windows 10 RTM','Server Enterprise (evaluation installation)'),
	(108,18,'10.0.10558','4','WINNT','Unknown','Enterprise'),
	(109,18,'6.0.6002','1','WINNT','Windows Vista with Service Pack 2','Ultimate'),
	(110,18,'10.0.10176','4','WINNT','Unknown','Enterprise'),
	(111,18,'6.0.6000','6','WINNT','Windows Vista','Business'),
	(112,18,'6.2.9200','8','WINNT','Windows 8 or Windows Server 2012','Server Datacenter (full installation)'),
	(113,18,'10.0.10144','101','WINNT','Unknown','Windows 8'),
	(114,18,'6.0.6000','3','WINNT','Windows Vista','Home Premium'),
	(115,18,'6.0.6001','6','WINNT','Windows Vista with Service Pack 1 or Windows Server 2008','Business'),
	(116,18,'6.1.7601','8','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Server Datacenter (full installation)'),
	(117,18,'10.0.10550','4','WINNT','Unknown','Enterprise'),
	(118,18,'6.2.8250','74','WINNT','Unknown',''),
	(119,18,'6.1.7601','33','WINNT','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1','Server Foundation'),
	(120,18,'6.2.8400','48','WINNT','Windows 8 Preview','Professional'),
	(121,18,'10.0.10122','48','WINNT','Unknown','Professional'),
	(122,18,'10.0.10159','101','WINNT','Unknown','Windows 8'),
	(123,18,'6.0.6001','2','WINNT','Windows Vista with Service Pack 1 or Windows Server 2008','Home Basic'),
	(124,18,'10.0.10130','4','WINNT','Unknown','Enterprise'),
	(125,18,'10.0.10560','101','WINNT','Unknown','Windows 8');
SET IDENTITY_INSERT [BI].[dimOperatingSystem] OFF
END