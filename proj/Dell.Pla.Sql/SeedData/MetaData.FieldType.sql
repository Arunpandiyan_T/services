﻿IF not EXISTS (SELECT  * from [MetaData].[FieldType])
BEGIN
insert into [MetaData].[FieldType] ([FieldTypeId],[Description]) values (0,'String');
insert into [MetaData].[FieldType] ([FieldTypeId],[Description]) values (1,'Number');
insert into [MetaData].[FieldType] ([FieldTypeId],[Description]) values (2,'Date');
insert into [MetaData].[FieldType] ([FieldTypeId],[Description]) values (3,'Boolean');
insert into [MetaData].[FieldType] ([FieldTypeId],[Description]) values (4,'Lookup');
insert into [MetaData].[FieldType] ([FieldTypeId],[Description]) values (5,'Lookup w. New Values');
insert into [MetaData].[FieldType] ([FieldTypeId],[Description]) values (6,'GUID');
END
