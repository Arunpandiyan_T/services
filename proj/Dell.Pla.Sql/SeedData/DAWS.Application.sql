﻿IF not EXISTS (SELECT  * from [DAWS].[Application])
BEGIN
SET IDENTITY_INSERT [DAWS].[Application] ON
insert into [DAWS].[Application] ([ApplicationId],[ApplicationName],[Description],[ApiKey],[SecretKey],[MayUpdateDeviceDetails],[isEnabled]) values (1,'Dell Update','The Dell Update Windows Application','1B0F4CF7-7673-44BE-9F96-2AF55A55B5F3','874EB60B-22E3-4DC1-9FB9-6E430342BFC3',1,1);
insert into [DAWS].[Application] ([ApplicationId],[ApplicationName],[Description],[ApiKey],[SecretKey],[MayUpdateDeviceDetails],[isEnabled]) values (3,'Cirrus Client','The Dell Digital Delivery Windows Application','3A816346-BED4-426A-B435-EC545117E892','36CED5A6-A018-4349-B600-23616CEC184E',1,1);
insert into [DAWS].[Application] ([ApplicationId],[ApplicationName],[Description],[ApiKey],[SecretKey],[MayUpdateDeviceDetails],[isEnabled]) values (5,'Tests','Unit and Load Tests','4BA5CAE9-8086-4855-B183-ADBAADD1D299','C553A28C-95E3-48EF-A852-6CA53C02562C',1,0);
insert into [DAWS].[Application] ([ApplicationId],[ApplicationName],[Description],[ApiKey],[SecretKey],[MayUpdateDeviceDetails],[isEnabled]) values (6,'DAWS Web API','The DAWS Web API Itself','D95F9E09-F688-46BF-ACBE-716355DAF99C','09127DAB-0F07-4FF1-B420-BF5101DBB102',1,1);
insert into [DAWS].[Application] ([ApplicationId],[ApplicationName],[Description],[ApiKey],[SecretKey],[MayUpdateDeviceDetails],[isEnabled]) values (7,'DAWS Web Portal','The DAWS Web Portal','8B74D7A5-64F8-423D-BE39-A07E67D5C4D0','1F9EEA22-F0CB-49CB-8D41-32E143AE9248',1,1);
insert into [DAWS].[Application] ([ApplicationId],[ApplicationName],[Description],[ApiKey],[SecretKey],[MayUpdateDeviceDetails],[isEnabled]) values (8,'MyLinux','MyLinux','357C531C-8892-458D-96B5-662E77F05518','2F20FDFD-328D-457F-BD33-CBB17C6D3780',1,1);
insert into [DAWS].[Application] ([ApplicationId],[ApplicationName],[Description],[ApiKey],[SecretKey],[MayUpdateDeviceDetails],[isEnabled]) values (9,'Dell Foundation Services','Dell Foundation Services','7D8B69CC-23B7-44D2-89BE-238BD784EE75','93DEB3BE-1802-4CB2-95F9-817A20E27C0A',1,1);
insert into [DAWS].[Application] ([ApplicationId],[ApplicationName],[Description],[ApiKey],[SecretKey],[MayUpdateDeviceDetails],[isEnabled]) values (10,'Learning Center','Learning Center','7C7C126A-09C0-4541-B814-4DCC7AAEA9AE','4E58C867-6E25-4B99-8D13-69F8F716DE2A',0,1);
insert into [DAWS].[Application] ([ApplicationId],[ApplicationName],[Description],[ApiKey],[SecretKey],[MayUpdateDeviceDetails],[isEnabled]) values (11,'Dell Data Services','Dell Data Services','4213F664-865C-4776-9B8D-921624BCA440','66461D01-A8DC-483F-A18D-69D2BDF8CFDD',0,1);
insert into [DAWS].[Application] ([ApplicationId],[ApplicationName],[Description],[ApiKey],[SecretKey],[MayUpdateDeviceDetails],[isEnabled]) values (12,'Kickstart','Kickstart 2.0','704BEF77-8EED-4119-8B15-50DF82733276','236BE345-D52D-4B2D-9914-ECA5F6AF2F19',0,1);
SET IDENTITY_INSERT [DAWS].[Application] OFF
END
