﻿IF not EXISTS (SELECT  * from [BI].[MapWindowsVersionToName])
BEGIN
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('10.0.10240','Windows 10 RTM');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('10.0.9888','Windows 10 Technical Preview (10.0.9888)');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('10.0.9896','Windows 10 Technical Preview (10.0.9896)');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('10.0.9897','Windows 10 Technical Preview (10.0.9897)');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('10.0.9904','Windows 10 Technical Preview (10.0.9904)');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('5.0.2195','Windows 2000');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('5.1.2600','Windows XP or Windows XP 64-Bit Edition Version 2002 (Itanium)');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('5.2.3790','Windows Server 2003 or Windows XP x64 Edition (AMD64/EM64T) or Windows XP 64-Bit Edition Version 2003 (Itanium)');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.0.6000','Windows Vista');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.0.6001','Windows Vista with Service Pack 1 or Windows Server 2008');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.0.6002','Windows Vista with Service Pack 2');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.1.7100','Windows 7 Preview');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.1.7600','Windows 7 or Windows Server 2008 R2');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.1.7601','Windows 7 with Service Pack 1 or Windows Server 2008 R2 with Service Pack 1');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.2.8400','Windows 8 Preview');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.2.9200','Windows 8 or Windows Server 2012');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.3.9200','Windows 8.1 or Windows Server 2012 R2');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.3.9431','Windows 8.1 Preview');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.3.9600','Windows 8.1 with Update 1');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.4.9841','Windows 10 Technical Preview (6.4.9841)');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.4.9860','Windows 10 Technical Preview (6.4.9860)');
insert into [BI].[MapWindowsVersionToName] ([WindowsVersion],[Name]) values ('6.4.9879','Windows 10 Technical Preview (6.4.9879)');
END
