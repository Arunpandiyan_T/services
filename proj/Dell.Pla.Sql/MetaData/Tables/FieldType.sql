﻿CREATE TABLE [MetaData].[FieldType] (
    [FieldTypeId] INT            NOT NULL,
    [Description] VARCHAR (2047) NOT NULL,
    CONSTRAINT [PK_MetaData_FieldType] PRIMARY KEY CLUSTERED ([FieldTypeId] ASC)
);

