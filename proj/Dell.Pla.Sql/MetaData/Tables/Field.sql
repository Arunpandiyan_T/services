﻿CREATE TABLE [MetaData].[Field] (
    [FieldId]          INT            IDENTITY (1, 1) NOT NULL,
    [FieldTypeId]      INT            DEFAULT ((0)) NOT NULL,
    [FieldNameIn]      NVARCHAR (64)  NOT NULL,
    [FieldNameSql]     NVARCHAR (64)  NULL,
    [FieldNameStorage] NVARCHAR (64)  NULL,
    [Description]      NVARCHAR (256) NULL,
    [ElementSql]       NVARCHAR (64)  NULL,
    [RangeMin]         NVARCHAR (256) NULL,
    [RangeMax]         NVARCHAR (256) NULL,
    [SpecialRule]      INT            DEFAULT ((0)) NULL,
    [isRequired]       BIT            DEFAULT ((0)) NULL,
    [isDeviceData]     BIT            DEFAULT ((0)) NULL,
    [isMultiple]       BIT            DEFAULT ((0)) NULL,
    [isAppend]         BIT            DEFAULT ((0)) NULL,
    [isDynamic]        BIT            DEFAULT ((0)) NULL,
    [isDeleted]        BIT            DEFAULT ((0)) NULL,
    [Expression]       NVARCHAR (MAX) NULL,
    [ApplicationId]    INT            DEFAULT ((0)) NULL,
    [IsEnriched]       BIT            DEFAULT ((0)) NULL,
    CONSTRAINT [PK_Field] PRIMARY KEY CLUSTERED ([FieldId] ASC),
    CONSTRAINT [FK_Field_FieldType] FOREIGN KEY ([FieldTypeId]) REFERENCES [MetaData].[FieldType] ([FieldTypeId])
);







