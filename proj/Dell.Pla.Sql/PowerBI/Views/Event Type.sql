﻿create view PowerBI.[Event Type]
as
SELECT [EventTypeId]
      ,[EventTypeName]
      ,[Description]
  FROM [DAWS].[EventType] with (nolock)