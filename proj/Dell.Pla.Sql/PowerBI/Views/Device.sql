﻿create view PowerBI.Device
as
SELECT [ServiceTag]
      ,[DeviceVersion]
      ,[DeviceCreationDate]
      ,[LastHeartbeatDate]
      ,[LastUpdateDateTime]
      ,[IsTestDevice]
      ,[CountryName]
      ,CountryIsWellDefined = case when [OtherCountryName] is null then 'Yes' else 'No' end
      ,[UserType]
  FROM [BI].[dimDevice]