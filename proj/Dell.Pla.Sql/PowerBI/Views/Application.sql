﻿create view PowerBI.[Application]
as
SELECT [ApplicationId]
      ,[ApplicationName]
      ,[Description]
      ,[MayUpdateDeviceDetails]
      ,[isEnabled]
  FROM [DAWS].[Application] with (nolock)