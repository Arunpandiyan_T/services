﻿create view PowerBI.Browser
as
SELECT [BrowserId]
      ,[BrowserName]
      ,[BrowserVersion]
      ,[BN]
      ,[BV]
	  ,BrowserFiltered = case when BrowserName like '%Internet Explorer' then 'IE'
	        when BrowserName like '%Google%Chrome%' then 'Chrome'
			when BrowserName = 'spark' then 'spark'
			when BrowserName = 'Firefox' then 'Firefox'
			when BrowserName = 'Opera Internet Browser' then 'Opera'
			else 'Other' end
  FROM [BI].[Browser]