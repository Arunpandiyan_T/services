﻿create view PowerBI.CPU
as
SELECT [CpuId]
      ,[CpuName]
      ,[CpuClass]
      ,[CpuMaker]
  FROM [BI].[Cpu]