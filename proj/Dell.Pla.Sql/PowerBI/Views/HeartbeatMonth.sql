﻿create view [PowerBI].[HeartbeatMonth]
as
select
		   MonthKey = HeartbeatMonth -- join to dimDate at month level
		  ,ServiceTag
		  ,[LanguageCultureName]
		  ,[Language] = substring([LanguageCultureName], 1, 2)
		  ,LanguageCountry = substring([LanguageCultureName], 4, 2)
		  ,[BiosRev]
		  ,[PlatformId]
		  ,[DefaultBrowserId]
		  ,[CpuId]
		  ,[OperatingSystemId]
		  ,cntDevice
  from (
	select HeartbeatMonth = YEAR(MonthKey) * 100 + MONTH( MonthKey )
		  ,ServiceTag
		  ,[LanguageCultureName]
		  ,[BiosRev]
		  ,[PlatformId]
		  ,[DefaultBrowserId]
		  ,[CpuId]
		  ,[OperatingSystemId]
		  ,monthRnk = row_number() over (partition by ServiceTag, YEAR(MonthKey) * 100 + MONTH( MonthKey ) order by MonthKey desc)
		  ,1 as cntDevice
	 from BI.factHeartbeatMonth
  ) hb
  where monthRnk = 1