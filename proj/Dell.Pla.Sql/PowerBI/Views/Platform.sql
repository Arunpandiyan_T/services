﻿create view PowerBI.[Platform]
as
SELECT [PlatformId]
      ,[PlatformName]
      ,[PlatformIdHex]
      ,[CodeName]
      ,[MarketingName]
	  ,PlatformPrefix = case when PNch > 0 then replace(substring(PlatformForPrefix, 1, PNch), '''','') else 'Other' end
	  ,MarketingPrefix = case when MKch > 0 then replace(substring(MarketingName, 1, MKch), '''','') else 'Other' end
  FROM (
	select *,
			PNch = PATINDEX( '% %', PlatformForPrefix),
			MKch = PATINDEX( '% %', MarketingName)
		FROM (select *,
			PlatformForPrefix =
			 case
				when MarketingName	like 'Dimension%'			then 'Dimension XX'
				when PlatformName		like 'DELL Dimension%'	then 'Dimension XX'
				when MarketingName	like 'DELL Precision%'		then 'Precision XX'
				when MarketingName	like 'DELL Precision%'		then 'Storage XX'
				when PlatformName	like 'M%'				then MarketingName
				when PlatformName	like 'Dell System Inspiron%' then 'Inspiron XX'
				when PlatformName	like 'Dell System Vostro%'	then 'Vostro XX'
				when PlatformName	like 'Dell System XPS%'		then 'XPS XX'
				else replace(PlatformName, '''','') end
			from [BI].[Platform]
		) x
	) pl
