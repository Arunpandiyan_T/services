﻿
create view [PowerBI].[Events]
as
SELECT
       [ServiceTag]
      ,[DeviceVersion]
      ,[EventTypeId]
      ,[ApplicationId]
      ,EventDate = convert(date, [EventDateTime])
	  ,EventTime = substring(convert(varchar(20), convert(time, [EventDateTime])),1,5)+':'+[EventDateTime2] 
      ,EventCreatedDate = convert(date, [EventCreatedTime])
	  ,EventCreatedTime = substring(convert(varchar(20), convert(time, [EventCreatedTime])),1,5)+':'+[EventCreatedTime2]
	  ,daysFromToday = datediff( day, EventCreatedTime, getUtcDate() )
      ,[AppVersion]
      ,[Url]
      ,[Filename]
      ,[FileVersion]
      ,[FileSize]
      ,[ExitCode]
      ,[ItemName]
      ,[UploadedFilename]
      ,[AdditionalData1]
      ,[AdditionalData2]
      ,[DynamicApplicationFieldsJSON]
      ,[EULA]
      ,[OobeCompleteDate]
      ,[LanguageCultureName]
	  ,[Language] = substring([LanguageCultureName], 1, 2)
	  ,LanguageCountry = substring([LanguageCultureName], 4, 2)
      ,[BiosRev]
      ,[GeoId]
      ,[PlatformId]
      ,[DefaultBrowserId]
      ,[CpuId]
      ,[OperatingSystemId]
      ,[SystemRamMb]
      ,[HardDriveGbFree]
      ,[IsTestDevice]
      ,[DotNetVersionList]
      ,[DynamicDeviceFieldsJSON]
	  ,WSB = case when EventTypeId in (1,2,3,5,6,29,31) then [AdditionalData1] end
	  ,WSB_5 = case when len([AdditionalData1]) >= 5 and EventTypeId in (1,2,3,5,6,29,31) then [AdditionalData1] end
		,cntDownloadSuccess = case when EventTypeId = 2 then 1 else 0 end
		,cntDownloadFailure = case when EventTypeId = 3 then 1 else 0 end
		,cntDownload = case when EventTypeId = 1 then 1 else 0 end
		,cntInstallationSuccess = case when EventTypeId in (29, 5) then 1
						  when EventTypeId = 31 then -1 else 0 end
		,cntInstallation = case when EventTypeId in (29, 5, 6) then 1 else 0 end
      ,CntAppCrashes = CASE WHEN EventTypeId = 10 then 1 else 0 end
  FROM [BI].[factEvent]