﻿CREATE VIEW [PowerBI].[HeartbeatMonthByApp]
	as
select
		   HeartbeatMonth = Year([MonthKey]) * 100 + Month(MonthKey)
		  ,ServiceTag
		  ,[ApplicationId]
		  ,[AppVersion]
		  ,[LanguageCultureName]
		  ,[Language] = substring([LanguageCultureName], 1, 2)
		  ,LanguageCountry = substring([LanguageCultureName], 4, 2)
		  ,[BiosRev]
		  ,[PlatformId]
		  ,[DefaultBrowserId]
		  ,[CpuId]
		  ,[OperatingSystemId]
		  ,cntDevice = 1
  from BI.factHeartbeatMonth