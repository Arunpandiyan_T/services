﻿CREATE FUNCTION [Util].[udf_Split]
 (
   @delimited nvarchar(max),
   @delimiter nvarchar(100)
 ) RETURNS @t TABLE
 (
 -- Id column can be commented out, not required for sql splitting string
   ID int identity(1,1), -- I use this column for numbering splitted parts
   Val nvarchar(max)
 )
 AS
 BEGIN
   declare @xml xml
   set @xml = N'<root><r>' + replace(@delimited,@delimiter,'</r><r>') + '</r></root>'

   insert into @t(Val)
   select 
     ltrim(rtrim( r.value('.','varchar(max)') )) as item
   from @xml.nodes('//root/r') as records(r)

   RETURN
 END

